<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

    /*********************************************
     * INDEX ROUTES
     ********************************************/
    Route::get('/', [
        'as'     => 'index',
        'uses'   => "HomeController@index",
    ]);

    Route::post('/api/artist/login', [
        'as' => 'artist.login',
        'uses' => 'AuthController@artistLogin'
    ]);
    Route::get('/api/artist/logout', [
        'as' => 'artist.login',
        'uses' => 'AuthController@artistLogout'
    ]);

    /*********************************************
    * ARTWORKS ROUTES
    ********************************************/

    Route::get('/api/artwork/gallery/{slug}', [
        'as'     => 'artwork.gallery',
        'uses'   => "ArtworkController@gallery",
    ]);
    Route::get('/api/artwork/paginate', [
        'as'     => 'artwork.paginate',
        'uses'   => "ArtworkController@getAll",
    ]);
    Route::get('/api/artwork/latest', [
        'as'     => 'artwork.latest',
        'uses'   => "ArtworkController@getLatest",
    ]);

    Route::get('/api/artwork/{is}/profile', [
        'as'     => 'artwork.profile',
        'uses'   => "ArtworkController@profile",
    ]);

    /*********************************************
     * ARTISTS ROUTES
     ********************************************/
    Route::get('/api/artist/latest', [
        'as'     => 'artist.latest',
        'uses'   => "ArtistController@getLatest",
    ]);

    Route::get('/api/artist/paginate', [
        'as'     => 'artist.paginate',
        'uses'   => "ArtistController@getAll",
    ]);

    Route::get('/api/artist/{slug}/preview', [
        'as'     => 'artist.preview',
        'uses'   => "ArtistController@preview",
    ]);

    Route::get('/api/artist/{slug}/profile', [
        'as'     => 'artist.profile',
        'uses'   => "ArtistController@profile",
    ]);

    /*********************************************
     * CONTACT ROUTES
     ********************************************/

    Route::post('/api/contact/send', [
        'as'     => 'contact.send',
        'uses'   => "HomeController@postContact",
    ]);

