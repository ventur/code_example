<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Landing Page Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'titles' => [
        'menu1' => 'Intro',
        'menu2' => 'We',
        'menu3' => 'You',
        'menu4' => 'Our artists',
        'menu5' => 'Contact',
    ],

    'menu1' => [
        'header' => 'Do you love art?',
        'text1' => 'Welcome to <strong>Articonic</strong>, the virtual gallery that will surprise you.',
        'text2' => 'Get ready to discover the new talents, very soon here, in Articonic.'
    ],

    'menu2' => [
        'header' => 'What do we do?',
        'text1' => 'Articonic aims to present <strong>the finest new talents</strong> in the art world',
    ],

    'menu3' => [
        'header' => 'Are you an artist?',
        'text1' => 'In Artionic you will can <strong>expose your artworks</strong> in our gallery and shell them easily',
    ],

    'menu4' => [
        'text1' => 'Do you want to be one of them?',
        'text2' => 'Or do you prefer buy one of their artworks?',
    ],

    'menu5' => [
        'header' => 'Contact us',
        'text1' => 'Do you want to be informed about our release?
                Do you want to be updated with the new talents?',
        'text2' => 'Just subscribe and don\'t lose any news.',
    ],

    'contact' => [
        'name' => 'Name',
        'surname' => 'Surname',
        'email' => 'Email',
        'message' => 'Do you want to tell us something?',
        'submit' => 'Send',
        'required' => "Please, write your email",
        'email_format' => "Please, write a valid email",
    ]
);