<?php

return array(
    'index' => 'Home',
    'artist' => 'Artists',
    'artwork' => 'Artworks ',
    'contact' => 'Contact',
    'logout' => 'Logout',
    'login' => 'Log in',
    'profile' => 'Profile',
    'admin' => 'Artworks',
    'new' => 'New',
);