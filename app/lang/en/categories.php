<?php

return array(
    'id' => [
        '1' => 'Others',
        '2' => 'Drawing',
        '3' => 'Sculpture',
        '4' => 'Photography',
        '5' => 'Digital',
    ],
    'all' => 'All'
);