<?php

return array(
    'welcome' => 'Welcome to Articonic',
    'slogan' => 'Exposing and offering talent',
    'last-artworks' => 'Last Artworks',
    'last-artists' => 'New Artists',
    'what' => [
        'title' => 'What is Articomic?',
        'body' => ' borns from the desire to create a unique space in which the visitor is introduced into
                    the world of art and inhibited of the rest of the world. We want to create a unique experience where
                    you can experience the feeling transmitted by the artworks as you can not feel in any other circumstances.
                    Let be conquered by the artworks and directed towards those unique feelings, let be catched by the artist\'s
                    ideas and look a completely new, different and unique world.
                    We don\'t want to be too sticky but that\'s the way to enjoy the art in a fulfilling way and
                    to understand it, each one with his way of thinking and seeing the world.
                    When we have succeeded in creating those feelings, even if you are the only one,
                    we will be pleased to have initiated this project.',
    ],
    'about' => [
        'title' => 'About us',
        'body' => 'We are two students who met in Brussels and decided to start this project for the
                    sake of art and this world so unique and special. With this project we want to
                    promote the quality and creativity and bring it to all our followers.
                    Thank you for taking a small piece of your time in Articonic porject. ',
    ],
    'gallery' => [
        'title' => 'Do you want to have your own Virtual Gallery?',
        'body' => 'If you are interested in joining this project and grow with us, please contact us!',
        'link' => 'Join Articonic!',
    ],

);