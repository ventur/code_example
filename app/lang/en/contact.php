<?php

return array(
    'title' => 'Contact us',
    'slogan' => 'Articonic listens to you',
    'body' => 'Are you an artist who wants to publish your artworks in Articonic?
            <br>Do you want to know all the news about Articonic?
            <br>Do you like our website? ¿Don\'t you like? Do you want to give your opinion?',
    'subtitle' => 'Whatever the reason... Please write!',
    'success' => 'Your message has been received, we will contact you shortly'
);