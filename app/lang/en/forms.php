<?php

return array(

    'label' => [
        'email' => 'Email',
        'female' => 'Female',
        'male' => 'Male',
        'message' => 'Message',
        'name' => 'Name',
        'no' => 'No',
        'show' => 'Show',
        'yes' => 'Yes',
    ],
    'button' => [
        'accept' => 'Accept',
        'enter' => 'Enter',
        'send' => 'Send',
        'picture' => 'Browse picture',
    ]
);