<?php

return array(
    'title' => 'Articonic',
    'slogan' => 'Actualiza tu perfil',
    'profile' => 'Tu perfil',
    'password' => 'Tu contraseña',
    'who' => 'Tus datos personales',
    'picture' => 'Fotografía',
    'password' => 'Cambia tu contraseña',
    'show' => 'Mostrar carácteres',

    'label' => [
        'name' => 'Name',
        'surname' => 'Surname',
        'gender' => 'Gender',
        'date_of_birth' => 'Date of birth',
        'address' => 'I live in...',
        'studies' => 'I studied in ...',
        'description' => 'Describe yourself',
        'my_art' => 'Describe your art',
        'picture' => 'Do you want to change your profile picture?',
        'old_password' => 'Current password',
        'new_password' => 'New password',
    ],
);