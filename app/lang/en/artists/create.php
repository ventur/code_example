<?php

return array(
    'title' => 'Welcome to Articonic',
    'slogan' => 'Configure your account',
    'who' => 'Who are you?',
    'about' => 'Talk about you',
    'picture' => 'Show your best face',
    'password' => 'Change your password',
    'slug' => 'Give a name to your Virtual Gallery',

    'label' => [
        'name' => 'Name',
        'surname' => 'Surname',
        'gender' => 'Gender',
        'date_of_birth' => 'Date of birth',
        'address' => 'I live in...',
        'studies' => 'I studied in ...',
        'description' => 'Describe yourself',
        'my_art' => 'Describe your art',
        'picture' => 'Choose a profile picture',
        'slug' => 'The name that you choose will be the address of your website , for example: "http://articonic.com/picasso"',
        'password' => 'Password',
        'password_confirmation' => 'Confirm password',
    ],
);