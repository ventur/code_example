<?php

return array(
    'title' => 'Your account',
    'slogan' => 'Log in and manage your artworks',
);