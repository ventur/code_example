<?php

return array(
    'title' => 'Articonic',
    'slogan' => 'Exposing and offering talent',
    'visit' => 'Start Virtual Visit ',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'age' => 'Age',
    'address' => 'Address',
    'studies' => 'Studies',
    'gallery' => 'Visit my gallery',
);