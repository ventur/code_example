<?php

return array(
    'title' => 'The Artworks of Articonic',
    'slogan' => 'We present the Artworks that give a sense to us',
);