<?php

return array(
    'title' => 'Articonic',
    'new' => 'Publish a new artwork',
    'edit' => 'Edit your artwork ',
    'profile' => 'Your artwork\'s profile',
    'picture' => 'Upload your artwork',
    'change' => 'Do you want to change the picture?',
    'formats' => 'Use: png, jpg, gif y bmp. Max: 5MB ',

    'label' => [
        'name' => 'Title',
        'description' => 'Description',
        'category' => 'Category',
        'thumb' => 'Select the thumbail',
        'watermark' => 'Do you want to include a watermark in your picture?',
        'technique' => 'Technique',
        'dimensions' => 'Dimensions',
    ],
);