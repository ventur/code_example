<?php

return array(
    'title' => 'Articonic',
    'slogan' => 'Welcome to the  ',
    'author' => 'Autor',
    'category' => 'Categoría',
    'technique' => 'Técnica',
    'dimensions' => 'Dimensiones',
    'age' => 'Precio',
    'no-available' => 'No disponible',
);