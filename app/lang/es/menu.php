<?php

return array(
    'index' => 'Inicio',
    'artist' => 'Artistas',
    'artwork' => 'Obras ',
    'contact' => 'Contacto',
    'logout' => 'Salir',
    'login' => 'Iniciar sesión',
    'profile' => 'Tu perfil',
    'admin' => 'Tus obras',
    'new' => 'Nueva obra',
);