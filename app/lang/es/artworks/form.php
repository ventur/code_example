<?php

return array(
    'title' => 'Articonic',
    'new' => 'Publica una nueva obra',
    'edit' => 'Edita tu obra ',
    'profile' => 'El perfil de tu obra',
    'picture' => 'Sube tu obra',
    'change' => '¿Quieres cambiar la imagen?',
    'formats' => 'Formatos aceptados: png, jpg, gif y bmp. Max: 5MB ',

    'label' => [
        'name' => 'Título',
        'description' => 'Descrípción',
        'category' => 'Categoría',
        'picture' => 'Elige una foto de perfil',
        'thumb' => 'Elige la miniatura de tu obra',
        'watermark' => '¿Quieres incluir una marca de agua en tu obra?',
        'technique' => 'Técnica',
        'dimensions' => 'Dimensiones',
    ],
);