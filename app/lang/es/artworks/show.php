<?php

return array(
    'title' => 'Articonic',
    'slogan' => 'Bienvenido a la galería de ',
    'author' => 'Autor',
    'category' => 'Categoría',
    'technique' => 'Técnica',
    'dimensions' => 'Dimensiones',
    'age' => 'Precio',
    'no-available' => 'No disponible',
);