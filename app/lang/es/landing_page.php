<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Landing Page Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'titles' => [
        'menu1' => 'Inicio',
        'menu2' => 'Nosotros',
        'menu3' => 'Tú',
        'menu4' => 'Nuestros artistas',
        'menu5' => 'Contacto',
    ],

    'menu1' => [
        'header' => '¿Te apasiona el arte?',
        'text1' => 'Bienvenido a <strong>Articonic</strong> la galería de arte virtual que te sorprenderá.',
        'text2' => 'Prepárate para descubrir los nuevos talentos, muy pronto aqui, en Articonic.'
    ],

    'menu2' => [
        'header' => '¿Qué hacemos?',
        'text1' => 'Articonic tiene como objetivo dar a conocer las <strong>mejores obras</strong> de los <strong>nuevos talentos</strong> del mundo del arte.',
    ],

    'menu3' => [
        'header' => '¿Eres un artista?',
        'text1' => 'En Artionic podrás <strong>exponer tus obras</strong> en nuestra gelaría y darles salida fácilmente',
    ],

    'menu4' => [
        'text1' => '¿Quieres ser uno de ellos?',
        'text2' => '¿O prefieres adquirir una de sus obras?',
    ],

    'menu5' => [
        'header' => 'Contáctanos',
        'text1' => '¿Quieres que te informemos sobre nuestro lanzamiento?
                ¿Quieres estar al día de los nuevos talentos?',
        'text2' => 'Subscríbete y no te pierdas ni una sola noticia.',
    ],

    'contact' => [
        'name' => 'Nombre',
        'surname' => 'Apellidos',
        'email' => 'Email',
        'message' => '¿Quieres decirnos algo?',
        'submit' => 'Enviar',
        'required' => "Por favor, introduzca su correo electrónico",
        'email_format' => "Por favor, introduce una dirección de correo válida",
    ]


);