<?php

return array(
    'title' => 'Articonic',
    'slogan' => 'Exponiendo y ofreciendo talento',
    'visit' => 'Iniciar visita virtual',
    'gender' => 'Sexo',
    'male' => 'Hombre',
    'female' => 'Mujer',
    'age' => 'Edad',
    'address' => 'Dirección',
    'studies' => 'Estudios',
    'gallery' => 'Entra en mi galería',
);