<?php

return array(
    'title' => 'Bienvenido a Articonic',
    'slogan' => 'Configuración de tu cuenta',
    'who' => '¿Quién eres?',
    'about' => 'Háblanos de ti',
    'picture' => 'Muestra tu mejor cara',
    'password' => 'Cambia tu contraseña',
    'slug' => 'Dale un nombre a tu galería virtual',

    'label' => [
        'name' => 'Nombre',
        'surname' => 'Apellidos',
        'gender' => 'Género',
        'date_of_birth' => 'Fecha de nacimiento',
        'address' => 'Vivo en...',
        'studies' => 'Estudié...',
        'description' => 'Descríbete a ti mismo',
        'my_art' => 'Describe tu arte',
        'picture' => 'Elige una foto de perfil',
        'slug' => 'El nombre elegido será la dirección a tu página web, por ejemplo: "http://articonic.com/picasso"',
        'password' => 'Contraseña',
        'password_confirmation' => 'Confirmar contraseña',
    ],
);