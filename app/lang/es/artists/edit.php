<?php

return array(
    'title' => 'Articonic',
    'slogan' => 'Actualiza tu perfil',
    'profile' => 'Tu perfil',
    'password' => 'Tu contraseña',
    'who' => 'Tus datos personales',
    'picture' => 'Fotografía',
    'password' => 'Cambia tu contraseña',
    'show' => 'Mostrar carácteres',

    'label' => [
        'name' => 'Nombre',
        'surname' => 'Apellidos',
        'gender' => 'Género',
        'date_of_birth' => 'Fecha de nacimiento',
        'address' => 'Vivo en...',
        'studies' => 'Estudié...',
        'description' => 'Descríbete a ti mismo',
        'my_art' => 'Describe tu arte',
        'picture' => '¿Quieres cambiar tu imagen de perfil?',
        'old_password' => 'Contraseña actual',
        'new_password' => 'Nueva contraseña',
    ],
);