<?php

return array(
    'title' => 'Contáctanos',
    'slogan' => 'Articonic te escucha',
    'body' => '¿Eres un artista y quieres publicar tus obras en Articonic?
            <br>¿Quieres estar al tanto de todas las novedades de Articonic?
            <br>¿Te gusta nuestra web? ¿No te gusta? ¿Quieres darnos tus sugenrencias?',
    'subtitle' => 'Sea cual sea el motivo... ¡no dudes en escribirnos!',
    'success' => 'Tu mensaje ha sido recibido, nos pondremos en contacto contigo lo antes posible'
);