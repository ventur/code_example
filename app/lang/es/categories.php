<?php

return array(
    'id' => [
        '1' => 'Otros',
        '2' => 'Pintura',
        '3' => 'Escultura',
        '4' => 'Fotografía',
        '5' => 'Digital',
    ],
    'all' => 'Todas'
);