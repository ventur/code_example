<?php

return array(
    'welcome' => 'Bienvenido a Articonic',
    'slogan' => 'Exponiendo y ofreciendo talento',
    'last-artworks' => ' Últimas Obras ',
    'last-artists' => ' Nuevos Artistas ',
    'what' => [
        'title' => '¿Qué es Articonic?',
        'body' => ' nace de las ganas de crear un espacio único en el que el visitante se introduzca en
                el mundo del arte y se inhiba del resto del mundo. Queremos que sea una experiencia única
                donde se pueda experimentar sensaciones transmitidas por las obras que no podrás sentir en
                ninguna otra circunstancia. Deja que las obras te conquisten y te dirijan hacia esas sensaciones
                únicas, que las ideas del artista consigan atraparte y llevarte a otro mundo completamente
                nuevo, diferente y único. No nos queremos poner demasiado pegajosos pero esa es la forma
                de disfrutar de una manera plena el arte, de conseguir entenderlo, cada uno con su forma de
                pensar y ver el mundo. Cuando hayamos conseguido crear esas sensaciones, aunque sea en
                uno sólo de vosotros, estaremos satisfechos de haber iniciado este proyecto.',
    ],
    'about' => [
        'title' => 'Sobre nosotros',
        'body' => 'Somos dos estudiantes que nos conocimos en Bruselas y que decidimos iniciar este proyecto
                por el gusto al arte y a este mundo tan único y especial. Con este proyecto queremos fomentar
                el gusto por la creatividad de calidad y transmitir ese concepto a todos nuestros seguidores.
                Gracias por dedicar un pequeño trozo de vuestro tiempo al proyecto Articonic.',
    ],
    'gallery' => [
        'title' => '¿Quieres tener tu propia galería virtual de arte?',
        'body' => 'Si estás interesado en formar parte de este proyecto y crecer con nosotros, no dudes en contactarnos',
        'link' => 'Únete a Articonic!',
    ],

);