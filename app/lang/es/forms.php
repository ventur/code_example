<?php

return array(

    'label' => [
        'email' => 'Correo electrónico',
        'female' => 'Femenino',
        'male' => 'Masculino',
        'message' => 'Mensaje',
        'name' => 'Nombre',
        'no' => 'No',
        'show' => 'Mostrar',
        'yes' => 'Sí',
    ],
    'button' => [
        'accept' => 'Aceptar',
        'enter' => 'Entrar',
        'send' => 'Enviar',
        'picture' => 'Buscar imagen',
    ]
);