<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration {

    /**
     * Create a ‘artists’ SQL table.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function(Blueprint $table) {

            // Primary key
            $table->increments('id');

            // Account data
            $table->string('email')->unique();
            $table->string('password');

            // Main user data
            $table->string('slug')->unique()->nullable();
            $table->string('name', 50)->default('');
            $table->string('surname', 50)->default('');
            $table->text('description')->nullable();
            $table->text('my_art')->nullable();
            $table->char('gender',1);
            $table->string('address')->nullable();
            $table->string('studies')->nullable();
            $table->boolean('birth')->default(false);
            $table->date('date_of_birth')->nullable();
            $table->string('picture')->nullable();

            //Active
            $table->boolean('active')->default(false);

            //UserInterface fields
            $table->string('remember_token', 100)->nullable();

            // Timestamps
            $table->nullableTimestamps();
        });
    }

    /**
     * Delete a ‘artists’ SQL table.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artists');
    }

}
