<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

    /**
     * Create a ‘categories’ SQL table.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function(Blueprint $table) {

            // Primary key
            $table->increments('id');

            //Main category data
            $table->string('name',50);

            // Timestamps
            $table->nullableTimestamps();
        });
    }

    /**
     * Drop a ‘categories’ SQL table.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }

}
