<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtworksTable extends Migration {

    /**
     * Create a ‘artworks’ SQL table.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artworks', function(Blueprint $table) {

            // Primary key
            $table->increments('id');

            // Foreign key
            $table->unsignedInteger('artist_id');
            $table->unsignedInteger('category_id');

            //Main artwork data
            $table->string('name',50);
            $table->text('description')->nullable();
            $table->string('dimensions')->nullable();
            $table->string('technique')->nullable();
            $table->string('picture');
            $table->integer('price')->default(0);
            $table->char('currency',3)->default('EUR');
            $table->boolean('revised')->default(false);

            // Timestamps
            $table->nullableTimestamps();
        });
    }

    /**
     * Drop a ‘artworks’ SQL table.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artworks');
    }

}
