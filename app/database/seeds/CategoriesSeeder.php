<?php

class CategoriesSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->delete();

        Category::create(array('name' => 'others'));
        Category::create(array('name' => 'painting'));
        Category::create(array('name' => 'sculpture'));
        Category::create(array('name' => 'photography'));
    }

}