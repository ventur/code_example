<?php namespace Articonic\helpers;

use PHPImageWorkshop\ImageWorkshop;

class ImageStorage {

    protected $scale = 200;

    public function handle($data)
    {
        ini_set('max_execution_time', 123456);
        $picture = $data['picture'];
        $coord = $data['coord'];
        $watermark = $data['watermark'];
        $artworkId = $data['artworkId'];

        //large
        mkdir(public_path('upload/artworks/' . $artworkId . '/large/'),0777,true);
        rename(public_path('upload/tmp/'.$picture),public_path('upload/artworks/' . $artworkId . '/large/0.png'));
        //$form->getPicture()->move(public_path('upload/artworks/' . $artwork->id . '/large'), '0.png');

        //medium
        $image = ImageWorkshop::initFromPath(public_path('upload/artworks/' . $artworkId . '/large/0.png'));
        $scale = $image->getWidth() / $this->scale;

        $image->resizeInPixel(1024, null, true);
        if ($watermark) {
            $watermarkLayer = ImageWorkshop::initFromPath(public_path('images/articonic.png'));
            $watermarkLayer->opacity(50);
            $watermarkLayer->resizeInPixel(null, $image->getHeight() * 0.8, true);
            $image->addLayerOnTop($watermarkLayer, 0, $image->getHeight() * 0.1, 'MT');
        }
        $image->save(public_path('upload/artworks/' . $artworkId . '/medium'), '0.png', true, null, 100);

        //thumb
        $image = ImageWorkshop::initFromPath(public_path('upload/artworks/' . $artworkId . '/large/0.png'));
        $image->cropInPixel($coord['w'] * $scale,
            $coord['h'] * $scale,
            $coord['x'] * $scale,
            $coord['y'] * $scale);
        $image->resizeInPixel(440, null, true);
        //$image->cropInPixel(440, 276, 0, 0, 'MM');
        if ($watermark) {
            $watermarkLayer = ImageWorkshop::initFromPath(public_path('images/articonic.png'));
            $watermarkLayer->opacity(50);
            $watermarkLayer->resizeInPixel(null, $image->getHeight() * 0.8, true);
            $image->addLayerOnTop($watermarkLayer, 0, $image->getHeight() * 0.1, 'MT');
        }
        $image->save(public_path('upload/artworks/' . $artworkId . '/thumb'), '0.png', true, null, 100);

        //watermark in large
        if ($watermark) {
            $image          = ImageWorkshop::initFromPath(public_path('upload/artworks/' . $artworkId . '/large/0.png'));
            $watermarkLayer = ImageWorkshop::initFromPath(public_path('images/articonic.png'));
            $watermarkLayer->opacity(50);
            $watermarkLayer->resizeInPixel(null, $image->getHeight() * 0.8, true);
            $image->addLayerOnTop($watermarkLayer, 0, $image->getHeight() * 0.1, 'MT');
            $image->save(public_path('upload/artworks/' . $artworkId . '/large'), '0.png', true, null, 100);
        }

    }
}
