<?php namespace Articonic\filters;

use \Illuminate\Config\Repository as Config;
use \Illuminate\Http\Request;
use \Illuminate\Routing\Route;
use \Illuminate\Session\SessionManager;

/**
 * Change the application locale if the lang of the request is supported.
 */
class LanguageFilter
{
    /**
     * The configuration repository.
     *
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * The session manager.
     *
     * @var \Illuminate\Session\SessionManager
     */
    protected $session;

    /**
     * The list of locales that the application supports.
     *
     * @var array
     */
    protected $supportedLocales = ['en', 'es'];

    /**
     * Constructor.
     *
     * @param  \Illuminate\Config\Repository       $config
     * @param  \Illuminate\Session\SessionManager  $session
     * @return void
     */
    public function __construct(Config $config, SessionManager $session)
    {
        // Inject the dependencies of the class.
        $this->config  = $config;
        $this->session = $session;
    }

    /**
     * Change the application locale if the lang of the request is supported.
     *
     * @param  \Illuminate\Routing\Route  $route
     * @param  \Illuminate\Http\Request   $request
     * @return void
     */
    public function filter(Route $route, Request $request)
    {
        $locale = $this->determineLanguage($request);

        $rootLang = $this->extractRootLanguage($locale);

        // Keep all the flash data in the session for the next request
        $this->session->reflash();

        // We will use one of the languages we found if one is supported.
        if ($lang = $this->pickSupportedLocale([$locale, $rootLang])) {

            // Save the language code in the session to persist this info. Make
            // an explicit save operation to trigger session sync right now.
            $this->session->set('lang', $lang);
            $this->session->save();

            // Runtime-change the locale of the application.
            $this->config->set('app.locale', $lang);
        }

        // Explicitly return void value to allow
        // the request to continue as normal.
        return;
    }

    /**
     * Determine what the requested language is.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function determineLanguage(Request $request)
    {
        if ($request->query->has('lang')) {

            // A ‘lang’ parameter has been provided via a query string
            return $request->query->get('lang');

        } elseif ($this->session->has('lang')) {

            // The session already stores a language code
            return $this->session->get('lang');
        }

        // If nothing was found till there, we check what language the
        // browser is accepting the most, based on HTTP headers.
        return $request->getPreferredLanguage();
    }

    /**
     * Extract the root language code of a locale.
     *
     * @param  string  $code
     * @return string
     */
    protected function extractRootLanguage($code)
    {
        // Example: fr_BE
        $regex = '#([A-Za-z]+)[_-]([A-Za-z]+)#';

        if (preg_match($regex, $code, $matches)) {
            $code = $matches[1];
        }

        return strtolower($code);
    }

    /**
     * Return the first element of a list that is a supported language, if any.
     *
     * @param  string|array  $codes  A list of language codes
     * @return string|false
     */
    protected function pickSupportedLocale($codes)
    {
        foreach ((array) $codes as $code) {

            if (in_array($code, $this->supportedLocales)) {
                return $code;
            }
        }

        return false;
    }
}
