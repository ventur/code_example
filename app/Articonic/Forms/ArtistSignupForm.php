<?php namespace Articonic\Forms;

class ArtistSignupForm extends AbstractForm
{
    /**
     * The validation rules related to login.
     *
     * @var array
     */
    protected $rules = [
        'email'    => 'required|email',
    ];

    /**
     * Get the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->inputData['email'];
    }

}
