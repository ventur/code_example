<?php namespace Articonic\Forms;


class ArtistUpdateForm extends AbstractForm
{
    /**
     * The validation rules related to create artwork.
     *
     * @var array
     */
    protected $rules = [
        'name'  => 'required|max:50',
        'surname'  => 'required|max:50',
        'gender'  => 'required',
        'birth'   => 'required',
        'date_of_birth'  => 'required',
    ];

    /**
     * Return the name
     * @return string
     */
    public function getName () {
        return $this->inputData['name'];
    }

    /**
     * Return the surname
     * @return string
     */
    public function getSurname () {
        return $this->inputData['surname'];
    }

    /**
     * Return gender
     * @return string
     */
    public function getGender () {
        return $this->inputData['gender'];
    }

    /**
     * Return date of birth
     * @return string
     */
    public function getDateOfBirth () {
        return $this->inputData['date_of_birth'];
    }

    /**
     * Return the address
     * @return string
     */
    public function getAddress () {
        return $this->inputData['address'];
    }

    /**
     * Return the studies
     * @return string
     */
    public function getStudies () {
        return $this->inputData['studies'];
    }

    /**
     * Return the description
     * @return string
     */
    public function getDescription () {
        return $this->inputData['description'];
    }

    /**
     * Return the my art description
     * @return string
     */
    public function getMyArt () {
        return $this->inputData['my_art'];
    }

    /**
     * Return the picture
     * @return string
     */
    public function getPicture () {
        return $this->inputData['picture'];
    }
}
