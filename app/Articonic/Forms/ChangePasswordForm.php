<?php namespace Articonic\Forms;

use Auth;

class ChangePasswordForm extends AbstractForm
{
    /**
     * The validation rules related to Change Password.
     *
     * @var array
     */
    protected $rules = [
        'old_password' => 'required',
        'new_password' => 'required|AlphaNum|min:8',
    ];

    /**
     * Return the old password
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->inputData['old_password'];
    }

    /**
     * Return the new password
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->inputData['new_password'];
    }

    /**
     * Validate the old password
     *
     * @return bool
     */
    public function validateCurrentPassword()
    {
        $credentials = ['email' => Auth::user()->email, 'password' => $this->getOldPassword()];
        if (Auth::validate($credentials)) {
            return true;
        }

        return false;
    }
}
