<?php namespace Articonic\Forms;


class LoginForm extends AbstractForm
{
    /**
     * The validation rules related to login.
     *
     * @var array
     */
    protected $rules = [
        'email' => 'required|email',
        'password' => 'required|AlphaNum|min:8',
    ];

    /**
     * Return the email
     * @return string
     */
    public function getEmail () {
        return $this->inputData['email'];
    }

    /**
     * Return the password
     * @return string
     */
    public function getPassword () {
        return $this->inputData['password'];
    }
}