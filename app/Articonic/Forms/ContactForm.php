<?php namespace Articonic\Forms;


class ContactForm extends AbstractForm
{
    /**
     * The validation rules related to create artwork.
     *
     * @var array
     */
    protected $rules = [
        'name'  => 'required',
        'email'  => 'required|email',
        'body'  => 'required',
    ];

    /**
     * Return the name
     * @return string
     */
    public function getName () {
        return $this->inputData['name'];
    }

    /**
     * Return the email
     * @return string
     */
    public function getEmail () {
        return $this->inputData['email'];
    }

    /**
     * Return message
     * @return string
     */
    public function getBody () {
        return $this->inputData['body'];
    }
}