<?php namespace Articonic\Forms;


class ArtworkForm extends AbstractForm
{
    /**
     * The validation rules related to create artwork.
     *
     * @var array
     */
    protected $rules = [
        'name'  => 'required',
        'category'  => 'required',
        'picture' => 'required',
        'watermark' => 'required',
    ];
    /**
     * Return the id
     * @return integer
     */
    public function getId ()
    {
        return $this->inputData['id'];
    }

    /**
     * Return the name
     * @return string
     */
    public function getName ()
    {
        return $this->inputData['name'];
    }

    /**
     * Return the description
     * @return string
     */
    public function getDescription () {
        return $this->inputData['description'];
    }

    /**
     * Return the category
     * @return string
     */
    public function getCategory()
    {
        return $this->inputData['category'];
    }

    /**
     * Return the picture
     * @return string
     */
    public function getPicture () {
        return $this->inputData['picture'];
    }

    /**
     * Return the watermark
     * @return string
     */
    public function getWatermark () {
        if ($this->inputData['watermark'] == 1)
            return true;
        return false;
    }

    /**
     * Return picture coordinates
     * @return array
     */
    public function getPictureCoord ()
    {
        return [
            'x' => $this->inputData['picture_x'],
            'y' => $this->inputData['picture_y'],
            'w' => $this->inputData['picture_w'],
            'h' => $this->inputData['picture_h'],
        ];
    }
}