<?php namespace Articonic\Forms;


class ArtistForm extends AbstractForm
{
    /**
     * The validation rules related to create artwork.
     *
     * @var array
     */
    protected $rules = [
        'name'  => 'required|max:50',
        'surname'  => 'required|max:50',
        'gender'  => 'required',
        'birth'   => 'required',
        'date_of_birth'  => 'required',
        'slug'  => 'required|alpha_dash|unique:artists|max:255',
        'picture' => 'required|image|max:5000',
        'password' => 'required|confirmed|min:8|max:20',
    ];

    /**
     * Return the name
     * @return string
     */
    public function getName () {
        return $this->inputData['name'];
    }

    /**
     * Return the surname
     * @return string
     */
    public function getSurname () {
        return $this->inputData['surname'];
    }

    /**
     * Return gender
     * @return boolean
     */
    public function getGender () {
        return $this->inputData['gender'];
    }

    /**
     * Return birth enable/disable
     * @return boolean
     */
    public function getBirth () {
        return $this->inputData['birth'];
    }

    /**
     * Return date of birth
     * @return string
     */
    public function getDateOfBirth () {
        return $this->inputData['date_of_birth'];
    }

    /**
     * Return the address
     * @return string
     */
    public function getAddress () {
        return $this->inputData['address'];
    }

    /**
     * Return the studies
     * @return string
     */
    public function getStudies () {
        return $this->inputData['studies'];
    }

    /**
     * Return the description
     * @return string
     */
    public function getDescription () {
        return $this->inputData['description'];
    }

    /**
     * Return the my art description
     * @return string
     */
    public function getMyArt () {
        return $this->inputData['my_art'];
    }

    /**
     * Return the slug
     * @return string
     */
    public function getSlug () {
        return $this->inputData['slug'];
    }

    /**
     * Return the picture
     * @return string
     */
    public function getPicture () {
        return $this->inputData['picture'];
    }

    /**
     * Return the password
     * @return string
     */
    public function getPassword () {
        return $this->inputData['password'];
    }
}