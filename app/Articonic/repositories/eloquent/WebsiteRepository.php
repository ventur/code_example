<?php namespace Articonic\Repositories\Eloquent;

use Articonic\Forms\ContactForm;
use Articonic\Repositories\WebsiteRepositoryInterface;

class WebsiteRepository implements WebsiteRepositoryInterface
{

    /**
     * Return artwork form
     *
     * @return ContactForm
     */
    public function getContactForm()
    {
        return new ContactForm();
    }

}