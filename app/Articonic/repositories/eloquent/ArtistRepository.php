<?php namespace Articonic\Repositories\Eloquent;

use Articonic\Forms\ArtistForm;
use Articonic\Forms\ArtistUpdateForm;
use Articonic\Forms\LoginForm;
use Articonic\Forms\ArtistSignupForm;
use Articonic\Forms\ChangePasswordForm;
use Articonic\Repositories\ArtistRepositoryInterface;
use Artist;
use Illuminate\Support\Str;

class ArtistRepository implements ArtistRepositoryInterface
{

    /**
     * A Artist Eloquent model.
     *
     * @var \Artwork
     */
    protected $artist;

    /**
     * Constructor.
     *
     * @param \Artist  $artist
     */
    public function __construct(Artist $artist)
    {
        $this->artist = $artist;
    }

    public function getPaginate($limit = 10) {
        return $this->artist->whereNotNull('slug')->orderBy('created_at','DESC')->paginate($limit);
    }

    /**
     * Create an Artist
     *
     * @param string $email
     * @param string $password
     *
     * @return Artist
     */
    public function createArtist($email, $password)
    {
        $artist = $this->artist->newInstance();
        $artist->email = $email;
        $artist->setPasswordAttribute($password);
        $artist->save();

        return $artist;
    }

    /**
     * Initialize an Artist
     *
     * @param Artist $artist
     * @param array  $data
     *
     * @return Artist
     */
    public function Initialize($artist, $data)
    {
        //Mandatory
        $artist->name = $data['name'];
        $artist->surname = $data['surname'];
        $artist->gender = $data['gender'];
        $artist->gender = $data['birth'];
        $artist->date_of_birth = $data['date_of_birth']['year'].'/'.$data['date_of_birth']['month'].'/'.$data['date_of_birth']['day'];
        $artist->picture = $artist->id.'.jpeg';

        //Slug
        $slug = $this->generateSlug(Str::slug($data['slug']));
        $artist->slug = $slug;

        //Password
        $artist->setPasswordAttribute($data['password']);

        //Optional
        if(array_key_exists('studies',$data)) {
            $artist->studies = $data['studies'];
        } else {
            $artist->studies = null;
        }
        if(array_key_exists('description',$data)) {
            $artist->description = $data['description'];
        } else {
            $artist->description = null;
        }
        if(array_key_exists('address',$data)) {
            $artist->address = $data['address'];
        } else {
            $artist->address = null;
        }
        if(array_key_exists('my_art',$data)) {
            $artist->my_art = $data['my_art'];
        } else {
            $artist->my_art = null;
        }

        $artist->save();

        return $artist;
    }

    /**
     * Update profile date of Artist
     *
     * @param Artist $artist
     * @param array  $data
     *
     * @return Artist
     */
    public function updateProfileData($artist, $data)
    {
        //Mandatory
        $artist->name = $data['name'];
        $artist->surname = $data['surname'];
        $artist->gender = $data['gender'];
        $artist->gender = $data['birth'];
        $artist->date_of_birth = $data['date_of_birth']['year'].'/'.$data['date_of_birth']['month'].'/'.$data['date_of_birth']['day'];

        //Optional
        if(array_key_exists('studies',$data)) {
            $artist->studies = $data['studies'];
        } else {
            $artist->studies = null;
        }
        if(array_key_exists('description',$data)) {
            $artist->description = $data['description'];
        } else {
            $artist->description = null;
        }
        if(array_key_exists('address',$data)) {
            $artist->address = $data['address'];
        } else {
            $artist->address = null;
        }
        if(array_key_exists('my_art',$data)) {
            $artist->my_art = $data['my_art'];
        } else {
            $artist->my_art = null;
        }

        $artist->save();

        return $artist;
    }


    /**
     * Generate a valid slug
     *
     * @param string $slug
     *
     * @return string
     */
    public function generateSlug($slug)
    {
        while ($this->artist->whereSlug($slug)->first()) {
            $slug = $slug."1";
        }
        return $slug;
    }

    /**
     * Find all artists
     *
     * @return array
     */
    public function findAllArtists() {
        return $this->artist->orderBy('name')->get();
    }

    /**
     * Get latest artworks
     *
     * @param int $limit
     *
     * @return array
     */
    public function getLatestArtists($limit = 12)
    {
        $artist = $this->artist->take($limit)->orderBy('created_at','DESC')->get();
        $artist = array_values(array_sort($artist, function($value)
        {
            return $value->name;
        }));
        return $artist;
    }

    /**
     * Get the Artist Sign Up form service.
     *
     * @return ArtistSignupForm
     */
    public function getArtistSignupForm()
    {
        return new ArtistSignupForm();
    }

    /**
     * Get the create Artist form service.
     *
     * @return ArtistForm
     */
    public function getArtistForm()
    {
        return new ArtistForm();
    }

    /**
     * Get the create Artist form service.
     *
     * @return ArtistUpdateForm
     */
    public function getArtistUpdateForm()
    {
        return new ArtistUpdateForm();
    }

    /**
     * Get the Login form service.
     *
     * @return LoginForm
     */
    public function getLoginForm()
    {
        return new LoginForm();
    }

    /**
     * Get the Login form service.
     *
     * @return ChangePasswordForm
     */
    public function getChangePasswordForm()
    {
        return new ChangePasswordForm();
    }
}