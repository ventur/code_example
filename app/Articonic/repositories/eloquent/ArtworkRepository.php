<?php namespace Articonic\Repositories\Eloquent;

use Articonic\Forms\ArtworkForm;
use Articonic\Repositories\ArtworkRepositoryInterface;
use Artwork;
use Category;
use Illuminate\Database\Eloquent\Collection;

class ArtworkRepository implements ArtworkRepositoryInterface
{

    /**
     * A Artwork Eloquent model.
     *
     * @var \Artwork
     */
    protected $artwork;

    /**
     * A Category Eloquent model.
     *
     * @var \Category
     */
    protected $category;

    /**
     * Constructor.
     *
     * @param \Artwork  $artwork
     * @param \Category  $category
     */
    public function __construct(Artwork $artwork, Category $category)
    {
        $this->artwork = $artwork;
        $this->category = $category;
    }

    /**
     * Find artist Artworks
     *
     * @param int $artistId Artist Id
     *
     * @return array
     */
    public function findByArtist($artistId)
    {
        return $this->artwork->whereArtistId($artistId)->whereRevised(true)->get();
    }

    /**
     * Find artist Artworks
     *
     * @param int $artistId Artist Id
     *
     * @return array
     */
    public function findOwnArtworks($artistId)
    {
        return $this->artwork->whereArtistId($artistId)->get();
    }

    /**
     * Find Artwork
     *
     * @param int $artworkId Id
     *
     * @return Artwork
     */
    public function find($artworkId)
    {
        return $this->artwork->find($artworkId);
    }


    /**
     * Create artwork
     *
     * @param array $data
     *
     * @return \Artwork
     */
    public function createArtwork(array $data)
    {
        $data = [
            'name' => $data['name'],
            'description' =>  array_key_exists('description',$data) ? $data['description'] : null,
            'technique' =>  array_key_exists('technique',$data) ? $data['technique'] : null,
            'dimensions' =>  array_key_exists('dimensions',$data) ? $data['dimensions'] : null,
            'category_id' => $data['category'],
            'artist_id' => Auth::user()->id,
            'picture' => '0.jpeg'
        ];
        $artwork = $this->artwork->newInstance($data);
        $artwork->save();

        return $artwork;
    }

    public function update($artwork, array $data)
    {
        $artwork->name = $data['name'];
        $artwork->description =  array_key_exists('description',$data) ? $data['description'] : null;
        $artwork->technique =  array_key_exists('technique',$data) ? $data['technique'] : null;
        $artwork->dimensions =  array_key_exists('dimensions',$data) ? $data['dimensions'] : null;
        $artwork->category_id = $data['category'];
        $artwork->save();

        return $artwork;
    }

    /**
     * Get all categories
     *
     * @return Collection
     */
    public function getCategories()
    {
        return $this->category->get();
    }

    /**
     * Get latest artworks
     *
     * @param int $limit
     *
     * @return array
     */
    public function getLatestArtworks($limit = 12)
    {
        $artworks = $this->artwork->whereRevised(true)->take($limit)->orderBy('created_at','DESC')->get();
        $artworks = array_values(array_sort($artworks, function($value)
        {
            return $value->name;
        }));
        return $artworks;
    }

    public function getPaginate($limit = 10) {
        return $this->artwork->whereRevised(true)->orderBy('created_at','DESC')->paginate($limit);
    }

    public function getGallery($artistId) {
        return $this->artwork->whereRevised(true)->whereArtistId($artistId)->orderBy('name')->get();
    }
    public function getById($artworkId,$revised = true)
    {
        $query = $this->artwork;
        if($revised){
            $query = $query->whereRevised(true);
        }
        return $query->whereId($artworkId)->first();
    }

    /**
     * Return artwork form
     *
     * @return ArtworkForm
     */
    public function getArtworkForm()
    {
        return new ArtworkForm();
    }

}