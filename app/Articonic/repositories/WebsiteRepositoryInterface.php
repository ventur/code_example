<?php namespace Articonic\Repositories;

use Articonic\Forms\ContactForm;

interface WebsiteRepositoryInterface
{
    /**
     * Return contact form
     *
     * @return ContactForm
     */
    public function getContactForm();
}