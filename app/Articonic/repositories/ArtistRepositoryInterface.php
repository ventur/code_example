<?php namespace Articonic\Repositories;

use Articonic\Forms\ArtistForm;
use Articonic\Forms\ArtistUpdateForm;
use Articonic\Forms\LoginForm;
use Articonic\Forms\ArtistSignupForm;
use Articonic\Forms\ChangePasswordForm;
use Artist;

interface ArtistRepositoryInterface
{

    public function getPaginate($limit = 10);

    /**
     * Create an Artist
     *
     * @param string $email
     * @param string $password
     *
     * @return Artist
     */
    public function createArtist($email, $password);

    /**
     * Initialize an Artist
     *
     * @param Artist $artist
     * @param array  $data
     *
     * @return Artist
     */
    public function Initialize($artist, $data);

    /**
     * Update profile date of Artist
     *
     * @param Artist $artist
     * @param array  $data
     *
     * @return Artist
     */
    public function updateProfileData($artist, $data);

    /**
     * Generate a valid slug
     *
     * @param string $slug
     *
     * @return string
     */
    public function generateSlug($slug);

    /**
     * Find all artists
     *
     * @return array
     */
    public function findAllArtists();

    /**
     * Get latest artworks
     *
     * @param int $limit
     *
     * @return array
     */
    public function getLatestArtists($limit = 12);

    /**
     * Get the Artist Sign Up form service.
     *
     * @return ArtistSignupForm
     */
    public function getArtistSignupForm();

    /**
     * Get the create Artist form service.
     *
     * @return ArtistForm
     */
    public function getArtistForm();

    /**
     * Get the create Artist form service.
     *
     * @return ArtistUpdateForm
     */
    public function getArtistUpdateForm();

    /**
     * Get the Login form service.
     *
     * @return LoginForm
     */
    public function getLoginForm();

    /**
     * Get the Login form service.
     *
     * @return ChangePasswordForm
     */
    public function getChangePasswordForm();
}