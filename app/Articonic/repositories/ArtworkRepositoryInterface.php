<?php namespace Articonic\Repositories;

use Articonic\Forms\ArtworkForm;
use Illuminate\Database\Eloquent\Collection;

interface ArtworkRepositoryInterface
{

    /**
     * Find artist Artworks
     *
     * @param int $artistId Artist Id
     *
     * @return array
     */
    public function findByArtist($artistId);

    /**
     * Find artist Artworks
     *
     * @param int $artistId Artist Id
     *
     * @return array
     */
    public function findOwnArtworks($artistId);

    /**
     * Find Artwork
     *
     * @param int $artworkId Id
     *
     * @return Artwork
     */
    public function find($artworkId);

    /**
     * Create artwork
     *
     * @param array $data
     *
     * @return mixed
     */
    public function createArtwork(array $data);

    /**
     * Update an artwork
     *
     * @param Artwork $artwork
     * @param array $data
     *
     * @return mixed
     */
    public function update($artwork, array $data);

    /**
     * Get all categories
     *
     * @return Collection
     */
    public function getCategories();

    /**
     * Get latest artworks
     *
     * @param int $limit
     *
     * @return array
     */
    public function getLatestArtworks($limit = 12);

    /**
     * Get artworks paginated
     *
     * @param int $limit
     *
     * @return array
     */
    public function getPaginate($limit = 10);

    /**
     * Get the artworks of an artist
     *
     * @param integer $artistId
     *
     * @return array
     */
    public function getGallery($artistId);

    /**
     * Get an artwork by id
     *
     * @param integer $artworkId
     * @param bool    $revised
     *
     * @return Artwork
     */
    public function getById($artworkId,$revised = true);

    /**
     * Return artwork form
     *
     * @return ArtworkForm
     */
    public function getArtworkForm();
}