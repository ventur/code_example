<?php namespace Articonic\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Articonic\Repositories\ArtistRepositoryInterface',
            'Articonic\Repositories\Eloquent\ArtistRepository'
        );

        $this->app->bind(
            'Articonic\Repositories\ArtworkRepositoryInterface',
            'Articonic\Repositories\Eloquent\ArtworkRepository'
        );

        $this->app->bind(
            'Articonic\Repositories\WebsiteRepositoryInterface',
            'Articonic\Repositories\Eloquent\WebsiteRepository'
        );

        // Change the application locale if the lang of the request is supported
        $this->app['router']->filter(
            'lang',
            'Articonic\filters\LanguageFilter'
        );
    }
}
