@extends('layouts.main')

@section('head')
<script src="/js/vendor/skeljs/skel.min.js">
    {
        prefix: "/css/index/main",
            resetCSS: true,
        useOrientation: true,
        boxModel: 'border',
        grid: { gutters: 30 },
        breakpoints: {
            wide: { range: "1200-", containers: 1140 },
            narrow: { range: "481-1000", containers: "fluid", grid: { collapse: true }   },
            mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
        }
    }
</script>
@stop

@section('content')
    {{--Sub Header --}}
    <!-- Title -->
    <div class="row title">
        <section class="12u">
            <h2>Registro</h2>
        </section>
    </div>

    {{-- Form --}}
<div class="row">
    {{-- MENU --}}
    @include('layouts.menu')
    <section class="-2u 3u">
        <hr>
        {{ Form::open([
        'route' => ['artist.signup.post'],
        'method' => 'post',
        ]) }}

        {{-- Email --}}
        <div>
            {{Form::label('email','Correo electrónico')}}
            {{ Form::text('email',Input::old('email')) }}

            @foreach ($errors->get('email') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        {{-- Submit --}}
        <div>
            {{ Form::button('Crear cuenta',['type' => 'submit', 'class' => 'button']) }}
        </div>
        {{ Form::close() }}
    </section>
</div>
@stop
