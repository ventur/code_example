@extends('layouts.main')

@section('head')
<script src="/js/vendor/skeljs/skel.min.js">
    {
        prefix: "css/index/main",
            resetCSS: true,
        useOrientation: true,
        boxModel: 'border',
        grid: { gutters: 30 },
        breakpoints: {
            wide: { range: "1200-", containers: 1140 },
            narrow: { range: "600-1000", containers: "fluid", grid: { collapse: false }   },
            mobile: { range: "-600", containers: "fluid", lockViewport: true, grid: { collapse: true } }
        }
    }
</script>
<link rel="stylesheet" type="text/css" href="/js/vendor/swipebox/css/swipebox.min.css" />
@stop

@section('content')

<!-- Title -->
<div class="row title">
    <section class="12u">
        <h2>@lang('artists/show.title')</h2>
        <p class="subtitle">@lang('artists/show.slogan')</p>
    </section>
</div>

{{-- MENU --}}
@include('layouts.menu')

<!-- Desktop -->
<div class="row">
    <div class="5u" style="text-align: center">
        <h3>{{$artist->name}} {{$artist->surname}}</h3>
    </div>
    <div class="7u" style="text-align: center">
        <a id="walk" class="button-play">@lang('artists/show.visit')<img src="/images/icons/play.svg"></a>
    </div>
</div>


{{-- DESKTOP AND TABLET VERSION --}}
<div class="row">
    <div class="5u">
        <img class="frame" src="/upload/artists/{{$artist->picture}}">
    </div>
    <div class="7u">
        <div class="square-black round content dark">
            <div class="row flush no-collapse">
                <div class="5u"><span class="subtitle">@lang('artists/show.gender'):</span></div>
                <div class="7u">{{$artist->gender=='m' ? trans('artists/show.male') : trans('artists/show.female')}}</div>
            </div>
            @if($artist->birth)
            <div class="12u"><hr></div>
            <div class="row flush no-collapse">
                <div class="5u"><span class="subtitle">@lang('artists/show.age'):</span></div>
                <div class="7u">{{(new DateTime())->diff(new DateTime($artist->date_of_birth))->y}}</div>
            </div>
            @endif
            @if($artist->address)
            <div class="12u"><hr></div>
            <div class="row flush">
                <div class="5u"><span class="subtitle">@lang('artists/show.address'):</span></div>
                <div class="7u">{{$artist->address}}</div>
            </div>
            @endif
            @if($artist->studies)
            <div class="12u"><hr></div>
            <div class="row flush">
                <div class="5u"><span class="subtitle">@lang('artists/show.studies'):</span></div>
                <div class="7u">{{$artist->studies}}</div>
            </div>
            @endif
        </div>
    </div>
</div>


{{-- DESCRIPTION --}}
@if($artist->description)
<div class="row">
    <section class="12u">
        <p class=" square-black round content dark">{{nl2br(htmlspecialchars(stripslashes($artist->description)))}}</p>
    </section>
</div>
@endif

{{-- GALERIA --}}
<div class="row">
    <div class="12u" style="text-align: center">
        <a href="{{route('artist.gallery',['slug' => $artist->slug])}}" class="square-link">@lang('artists/show.gallery')</a>
    </div>
</div>


<script src="/js/vendor/swipebox/js/jquery.swipebox.min.js"></script>

<script type="text/javascript">
    document.getElementById('walk').onclick = function(e) {
        e.preventDefault();
        $.swipebox( [
            @foreach($artworks as $artwork)
            { href:'upload/artworks/{{$artwork->id}}/medium/{{$artwork->picture}}', title:'{{$artwork->name}}' },
            @endforeach
        ] );
    };
</script>
@stop
