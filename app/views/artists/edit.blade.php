@extends('layouts.main')

@section('head')
<script src="/js/vendor/skeljs/skel.min.js">
    {
        prefix: "/css/index/main",
            resetCSS: true,
        useOrientation: true,
        boxModel: 'border',
        grid: { gutters: 30 },
        breakpoints: {
            wide: { range: "1200-", containers: 1140 },
            narrow: { range: "481-1000", containers: "fluid", grid: { collapse: true }   },
            mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
        }
    }
</script>
@stop

@section('content')
{{--Sub Header --}}
<!-- Title -->
<div class="row title">
    <section class="12u">
        <h2>@lang('artists/edit.title')</h2>
        <p class="subtitle">@lang('artists/edit.slogan')</p>
    </section>
</div>

{{-- MENU --}}
@include('layouts.menu')

{{-- Form --}}
<div class="row">
    <section class="-2u 8u">
        {{-- TABS --}}
        <div class="row flush">
            <section class="12u" >
                <div class="row flush no-collapse tab-frame">
                    <div id="tab-one" class="6u tab {{$tab == 1? 'active': ''}}">
                        <h5>@lang('artists/edit.profile')</h5>
                    </div>
                    <div  id="tab-two" class="6u tab {{$tab == 2? 'active': ''}}" style="border-radius: 25px 0 0 0;">
                        <h5>@lang('artists/edit.password')</h5>
                    </div>
                </div>
            </section>
        </div>

        {{-- PERFIL --}}
        <section id="section-one" class="12u square-black round-bottom" style="display: {{$tab == 1? 'block': 'none'}}">
            {{ Form::open([
            'route' => 'artist.update',
            'method' => 'post',
            'files' => true
            ]) }}
            <h3>@lang('artists/edit.who')</h3>
            {{-- Name and surname --}}
            <div>
                {{Form::label('name',trans('artists/edit.label.name'))}}
                {{ Form::text('name',Input::old('name',$artist->name)) }}
                @foreach ($errors->get('name') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach

                {{Form::label('surname',trans('artists/edit.label.surname'))}}
                {{ Form::text('surname',Input::old('surname',$artist->surname)) }}
                @foreach ($errors->get('surname') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach

                {{Form::label('gender',trans('artists/edit.label.gender'))}}
                {{Form::radio('gender','m', $artist->gender == 'm' ? true : false)}} <span class="dark">@lang('forms.label.male')</span>
                {{Form::radio('gender','f', $artist->gender == 'f' ? true : false)}} <span class="dark">@lang('forms.label.female')</span>
                @foreach ($errors->get('gender') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach

                {{Form::label('date_of_birth',trans('artists/edit.label.date_of_birth'))}}
                <span class="dark">@lang('forms.label.show')</span>
                {{Form::radio('birth',true, $artist->birth == true ? true : false)}} <span class="dark">Sí</span>
                {{Form::radio('birth',false, $artist->birth == false ? true : false)}} <span class="dark">No</span>
                <div class="form-inline">
                    {{ Form::selectRange('date_of_birth[day]', 1, 31, Input::old('date_of_birth[day]',$birthday[2])) }}
                    {{ Form::selectMonth('date_of_birth[month]', Input::old('date_of_birth[month]',$birthday[1])) }}
                    {{ Form::selectYear('date_of_birth[year]', date('Y') - 1, date('Y') - 150, Input::old('date_of_birth[year]',$birthday[0])) }}
                </div>
                @foreach ($errors->get('date_of_birth') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach
            </div>

            <hr>
            <br>
            <h3>@lang('artists/edit.profile')</h3>

            {{-- Address --}}
            {{Form::label('address',trans('artists/edit.label.address'))}}
            {{ Form::text('address',Input::old('address',$artist->address)) }}
            @foreach ($errors->get('address') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach

            {{-- Studies --}}
            {{Form::label('studies',trans('artists/edit.label.studies'))}}
            {{ Form::text('studies',Input::old('studies',$artist->studies)) }}
            @foreach ($errors->get('studies') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach

            {{-- Description --}}
            <div>
                {{Form::label('description',trans('artists/edit.label.description'))}}
                {{ Form::textArea('description',Input::old('description',$artist->description)) }}

                @foreach ($errors->get('description') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach
            </div>

            {{-- My art --}}
            <div>
                {{Form::label('my_art',trans('artists/edit.label.my_art'))}}
                {{ Form::textArea('my_art',Input::old('my_art',$artist->my_art)) }}

                @foreach ($errors->get('my_art') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach
            </div>

            <hr>
            <br>
            <h3>@lang('artists/edit.picture')</h3>

            {{-- Picture --}}
            <div>
                <img style="width: 100%" class="frame" src="/upload/artists/thumbs/{{Auth::user()->id}}.jpeg">
                {{Form::label('picture',trans('artists/edit.label.picture'))}}
                {{ Form::file('picture', ['class' => 'dark','style'=>'block'])}}
                @foreach ($errors->get('picture') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach
            </div>

            {{-- Submit --}}
            <div>
                <br>
                {{ Form::button(trans('forms.button.accept'),['type' => 'submit', 'class' => 'button']) }}
            </div>
            {{ Form::close() }}
        </section>

        {{-- PASSWORD --}}
        <section id="section-two" class="12u square-black round-bottom" style="display: {{$tab == 2? 'block': 'none'}}">
            {{ Form::open([
            'route' => 'artist.password.change',
            'method' => 'post'
            ]) }}
            @if(Session::has('password_changed'))
            <div class="dark">
                <p>{{ Session::get('password_changed') }}</p>
            </div>
            @endif
            <div>
                {{ Form::label("old_password", trans('artists/edit.label.old_password')) }}
                {{ Form::password("old_password") }}
                @foreach ($errors->get('old_password') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach
                @if(Session::has('password_mismatch'))
                <p class="error">{{ Session::get('password_mismatch') }}</p>
                @endif
            </div>
            <div>
                {{ Form::label("new_password", trans('artists/edit.label.new_password')) }}
                {{ Form::password("new_password") }}
                @foreach ($errors->get('new_password') as $error)
                <p class="error">{{ $error }}</p>
                @endforeach
                {{ Form::checkbox('show_password','1', null,['id'=>'show_password'])}}
                <span class="dark">@lang('artists/edit.show')</span>
            </div>
            
            {{-- Submit --}}
            <div>
                <br>
                {{ Form::button(trans('forms.button.accept'),['type' => 'submit', 'class' => 'button']) }}
            </div>
            {{ Form::close() }}
        </section>
    </section>
</div>

<script type="application/javascript">
    $( document ).ready(function() {
        var tabs = $(".tab");
        // Click Handler
        tabs.click(function( e ) {
            e.preventDefault();
            if ( $(this).attr('id') == "tab-one") {
                $('#section-one').show();
                $(this).addClass('active');
                $('#section-two').hide();
                $('#tab-two').removeClass('active');
            } else {
                $('#section-one').hide();
                $('#tab-one').removeClass('active');
                $('#section-two').show();
                $(this).addClass('active');
            }
        });
    });
</script>

<script type="application/javascript">
    var checkbox = document.getElementById('show_password'),
        pwdField = document.getElementById('new_password');

    checkbox.addEventListener('change', function (e) {
        pwdField.type = this.checked ? 'text' : 'password';
    });
</script>
@stop
