@extends('layouts.main')

@section('head')
<script src="/js/vendor/skeljs/skel.min.js">
    {
        prefix: "/css/index/main",
            resetCSS: true,
        useOrientation: true,
        boxModel: 'border',
        grid: { gutters: 30 },
        breakpoints: {
            wide: { range: "1200-", containers: 1140 },
            narrow: { range: "481-1000", containers: "fluid", grid: { collapse: true }   },
            mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
        }
    }
</script>
@stop

@section('content')
{{--Sub Header --}}
<!-- Title -->
<div class="row title">
    <section class="12u">
        <h2>@lang('artists/create.title')</h2>
        <p class="subtitle">@lang('artists/create.slogan')</p>
    </section>
</div>

{{-- MENU --}}
@include('layouts.menu')

{{-- Form --}}
<div class="row">
    <section class="-2u 8u">
        {{ Form::open([
        'route' => 'artist.store',
        'method' => 'post',
        'files' => true
        ]) }}
        <h3>@lang('artists/create.who')</h3>
        {{-- Name and surname --}}
        <div>
            {{Form::label('name',trans('artists/create.label.name'))}}
            {{ Form::text('name',Input::old('name')) }}
            @foreach ($errors->get('name') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach

            {{Form::label('surname',trans('artists/create.label.surname'))}}
            {{ Form::text('surname',Input::old('surname')) }}
            @foreach ($errors->get('surname') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach

            {{Form::label('gender',trans('artists/create.label.gender'))}}
            {{Form::radio('gender','m')}} <span class="dark">@lang('forms.label.male')</span>
            {{Form::radio('gender','f')}} <span class="dark">@lang('forms.label.female')</span>
            @foreach ($errors->get('gender') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach

            {{Form::label('date_of_birth',trans('artists/create.label.date_of_birth'))}}
            <span class="dark">@lang('forms.label.show')</span>
            {{Form::radio('birth',true, false)}} <span class="dark">@lang('forms.label.yes')</span>
            {{Form::radio('birth',false, false)}} <span class="dark">@lang('forms.label.no')</span>
            <div class="form-inline">
                {{ Form::selectRange('date_of_birth[day]', 1, 31, null) }}
                {{ Form::selectMonth('date_of_birth[month]', null) }}
                {{ Form::selectYear('date_of_birth[year]', date('Y') - 1, date('Y') - 150, null) }}
            </div>
            @foreach ($errors->get('date_of_birth') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        <hr>
        <br>
        <h3>@lang('artists/create.about')</h3>
        {{-- Address --}}
        {{Form::label('address',trans('artists/create.label.address'))}}
        {{ Form::text('address',Input::old('address')) }}
        @foreach ($errors->get('address') as $error)
        <p class="error">{{ $error }}</p>
        @endforeach

        {{-- Studies --}}
        {{Form::label('studies',trans('artists/create.label.studies'))}}
        {{ Form::text('studies',Input::old('studies')) }}
        @foreach ($errors->get('studies') as $error)
        <p class="error">{{ $error }}</p>
        @endforeach

        {{-- Description --}}
        <div>
            {{Form::label('description',trans('artists/create.label.description'))}}
            {{ Form::textArea('description',Input::old('description')) }}

            @foreach ($errors->get('description') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        {{-- My art --}}
        <div>
            {{Form::label('my_art',trans('artists/create.label.my_art'))}}
            {{ Form::textArea('my_art',Input::old('my_art')) }}

            @foreach ($errors->get('my_art') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        <hr>
        <br>
        <h3>@lang('artists/create.picture')</h3>
        {{-- Picture --}}
        <div>
            {{Form::label('picture',trans('artists/create.label.picture'))}}
            {{ Form::file('picture', ['class' => 'dark','style'=>'block'])}}
            @foreach ($errors->get('picture') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        <hr>
        <br>
        <h3>@lang('artists/create.slug')</h3>
        {{-- Slug --}}
        <div>
            {{Form::label('slug',trans('artists/create.label.slug'))}}
            {{ Form::text('slug',Input::old('slug')) }}
            @foreach ($errors->get('slug') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        <hr>
        <br>
        <h3>@lang('artists/create.password')</h3>
        {{-- Password --}}
        <div>
            {{Form::label('password',trans('artists/create.label.password'))}}
            {{ Form::password('password') }}

            @foreach ($errors->get('password') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        {{-- Password Confirmation --}}
        <div>
            {{Form::label('password_confirmation',trans('artists/create.label.password_confirmation'))}}
            {{ Form::password('password_confirmation') }}

            @foreach ($errors->get('password_confirmation') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>
        <hr>
        <br>
        {{-- Submit --}}
        <div>
            {{ Form::button(trans('forms.button.enter'),['type' => 'submit', 'class' => 'button']) }}
        </div>
        {{ Form::close() }}
    </section>
</div>
@stop
