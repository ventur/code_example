@extends('layouts.main')

@section('head')
<script src="/js/vendor/skeljs/skel.min.js">
    {
        prefix: "/css/index/main",
        resetCSS: true,
        useOrientation: true,
        boxModel: 'border',
        grid: { gutters: 30 },
        breakpoints: {
            wide: { range: "1200-", containers: 1140 },
            narrow: { range: "481-1000", containers: "fluid", grid: { collapse: true }   },
            mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
        }
    }
</script>
@stop

@section('content')
    {{--Sub Header --}}
    <!-- Title -->
    <div class="row title">
        <section class="12u">
            <h2>@lang('artists/login.title')</h2>
            <p class="subtitle">@lang('artists/login.slogan')</p>
        </section>
    </div>

    {{-- Form --}}
<div class="row flush">
    {{-- MENU --}}
    @include('layouts.menu')
    <section class="-2u 3u">
        {{ Form::open([
        'route' => ['artist.login.post'],
        'method' => 'post',
        ]) }}

        {{-- Email --}}
        <div>
            {{Form::label('email','Correo electrónico')}}
            {{ Form::email(
            'email',
            Input::old('email'),
            ['id' => 'email']
            ) }}
            @foreach ($errors->get('email') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>

        {{-- Password --}}
        <div>
            {{Form::label('password','Contraseña')}}
            {{ Form::password('password',['id' => 'password']) }}
            @foreach ($errors->get('password') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
            @if(Session::has('auth_fail'))
            <p class="error">{{ Session::get('auth_fail') }}</p>
            @endif
        </div>

        {{-- Submit --}}
        <div>
            <br>
            {{ Form::button('Entrar',['type' => 'submit', 'class' => 'button']) }}
        </div>
        {{ Form::close() }}
    </section>
</div>
@stop
