@extends('layouts.main')

@section('head')
<script src="/js/vendor/skeljs/skel.min.js">
    {
        prefix: "/css/index/main",
        resetCSS: true,
        useOrientation: true,
        boxModel: 'border',
        grid: { gutters: 30 },
        breakpoints: {
            wide: { range: "1200-", containers: 1140 },
            narrow: { range: "481-1000", containers: "fluid", grid: { collapse: true }   },
            mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
        }
    }
</script>
<link rel="stylesheet" type="text/css" href="/js/vendor/swipebox/css/swipebox.min.css" />
@stop

@section('content')

<!-- Title -->
<div class="row title">
    <section class="12u">
        <h2><a href="{{route('artist.show',[$artist->slug])}}">{{$artist->name}} {{$artist->surname}}</a></h2>
        <p class="subtitle">@lang('artists/gallery.slogan')</p>
    </section>
</div>

{{-- MENU --}}
@include('layouts.menu')

<div class="row">
    <section class="-1u 10u dark">
        <p>{{$artist->my_art}}</p>
    </section>
</div>

{{-- FILTERS --}}
<div class="row">
    <section class="-1u 10u">
        <ul id="filters" class="actions">
            <li class="all active" data-filter="all">@lang('categories.all')</li>
            @foreach($categories as $category)
            <li data-filter="filter-{{$category->id}}">@lang('categories.id.'.$category->id)</li>
            @endforeach
        </ul>
    </section>
</div>

{{-- IMAGES --}}
<div class="row">
    <section id="wookmark" class="12u">
        <ul id="tiles">
            @foreach($artworks as $artwork)
            <li data-filter-class='["filter-{{$artwork->category_id}}"]'>
                <a class="image img-light" href="{{route('artwork.show',array($artwork->id))}}" title="{{$artwork->name}}">
                    <img class="image full" src="/upload/artworks/{{$artwork->id}}/medium/{{$artwork->picture}}" alt="" />
                </a>
                <p style="font-size: 1em; text-align: center">{{$artwork->name}}</p>
                <div class="row no-collapse flush">
                    <a class="-1u 2u swipebox image img-light" href="/upload/artworks/{{$artwork->id}}/medium/{{$artwork->picture}}" title="{{$artwork->name}}">
                        <img class="icon" src="/images/icons/eye.svg" title="view" alt="" />
                    </a>
                    <a class="-7u 2u image img-light" href="{{route('artwork.show',array($artwork->id))}}" title="{{$artwork->name}}">
                        <img class="icon" src="/images/icons/description.svg" title="page" alt="" />
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </section>
</div>

<!-- Include the imagesLoaded plug-in -->
<script src="/js/vendor/swipebox/js/jquery.imagesloaded.js"></script>
<script src="/js/vendor/swipebox/js/jquery.wookmark.js"></script>
<script src="/js/vendor/swipebox/js/jquery.swipebox.min.js"></script>

<!-- Once the page is loaded, initalize the plug-in. -->
<script type="text/javascript">
    jQuery(function($) {
        $(".swipebox").swipebox();
    });
</script>

<!-- Once the page is loaded, initalize the plug-in. -->
<script type="text/javascript">
    (function ($){
        $('#tiles').imagesLoaded(function() {

            // Prepare layout options.
            var options = {
                autoResize: true, // This will auto-update the layout when the browser window is resized.
                container: $('#wookmark'), // Optional, used for some extra CSS styling
                offset: 2, // Optional, the distance between grid items
                itemWidth: 210, // Optional, the width of a grid item
                fillEmptySpace: true // Optional, fill the bottom of each column with widths of flexible height
            };

            // Get a reference to your grid items.
            var handler = $('#tiles li'),
                filters = $('#filters li');

            // Call the layout function.
            handler.wookmark(options);

            /**
             * When a filter is clicked, toggle it's active state and refresh.
             */
            var onClickFilter = function(event) {
                var item = $(event.currentTarget),
                    activeFilters = [],
                    filterType = item.data('filter');

                item.toggleClass('active');

                // Collect active filter strings
                if (filterType === 'all') {
                    filters.removeClass('active');
                    $('.all').addClass('active');
                } else {
                    $item.toggleClass('active');
                    $('.all').removeClass('active');

                    // Collect active filter strings
                    filters.filter('.active').each(function() {
                        activeFilters.push($(this).data('filter'));
                    });

                    if(filters.filter('.active').length==0) {
                        $('.all').addClass('active');
                    }
                }

                handler.wookmarkInstance.filter(activeFilters, 'or');
            }

            // Capture filter click events.
            filters.click(onClickFilter);
        });
    })(jQuery);
</script>
@stop
