@extends('layouts.main')

@section('head')
<script src="/js/vendor/jquery/jquery.min.js"></script>
<script src="/js/vendor/jcrop/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="/js/vendor/jcrop/css/jquery.Jcrop.css" type="text/css" />

<script src="/js/vendor/skeljs/skel.min.js">
    {
        prefix: "/css/index/main",
            resetCSS: true,
        useOrientation: true,
        boxModel: 'border',
        grid: { gutters: 30 },
        breakpoints: {
            wide: { range: "1200-", containers: 1140 },
            narrow: { range: "481-1000", containers: "fluid", grid: { collapse: true }   },
            mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
        }
    }
</script>
@stop

@section('content')
{{--Sub Header --}}
<!-- Title -->
<div class="row title">
    <section class="12u">
        <h2>@lang('artworks/form.title')</h2>
        <p class="subtitle">@lang('artworks/form.edit') "{{$artwork->name}}"</p>
    </section>
</div>

{{-- MENU --}}
@include('layouts.menu')

{{-- Form --}}
<div class="row">
    <section class="-2u 8u">
        {{ Form::open([
        'route' => ['artwork.update'],
        'method' => 'post',
        'files' => true
        ]) }}
        <h3>@lang('artworks/form.profile')</h3>
        <div>
            {{Form::label('name',trans('forms.label.name'))}}
            {{ Form::text('name', Input::old('name',$artwork->name))}}
            @foreach ($errors->get('name') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>
        <div>
            {{Form::label('description',trans('artworks/form.label.description'))}}
            {{ Form::textArea('description', Input::old('description',$artwork->description))}}
            @foreach ($errors->get('description') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>
        <div class="dark">
            {{Form::label('category',trans('artworks/form.label.category'))}}
            {{ Form::radio('category',5, $artwork->category_id == 5 ? true : false)}} {{trans('categories.id.5')}}
            {{ Form::radio('category',3, $artwork->category_id == 3 ? true : false)}} {{trans('categories.id.3')}}
            {{ Form::radio('category',4, $artwork->category_id == 4 ? true : false)}} {{trans('categories.id.4')}}
            {{ Form::radio('category',2, $artwork->category_id == 2 ? true : false)}} {{trans('categories.id.2')}}
            {{ Form::radio('category',1, $artwork->category_id == 1 ? true : false)}} {{trans('categories.id.1')}}
            @foreach ($errors->get('picture') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>
        <div>
            {{ Form::label('technique',trans('artworks/form.label.technique'))}}
            {{ Form::text('technique', Input::old('technique', $artwork->technique))}}
            @foreach ($errors->get('technique') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>
        <div>
            {{ Form::label('dimensions',trans('artworks/form.label.dimensions'))}}
            {{ Form::text('dimensions', Input::old('dimensions', $artwork->dimensions))}}
            @foreach ($errors->get('dimensions') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>
        <hr>
        <br>
        <h3>@lang('artworks/form.change')</h3>
        <div>
            <div id="label_picture" style="display: none">
                {{Form::label('thumbail',trans('artworks/form.label.thumb'))}}
            </div>
            <div>
                <img class="frame" src="/upload/artworks/{{$artwork->id}}/medium/{{$artwork->picture}}" id="main_picture" name="picture-single" style=" max-width: 15em">
            </div>
            <div id="pictureError"></div>
            <div class="uploadIframe">
                <iframe src="/picture/upload" width="200px" height="30px" frameBorder="0" scrolling="no" display="block"></iframe>
            </div>
            <p class="dark">@lang('artworks/form.formats')</p>

            @foreach ($errors->get('picture') as $error)
            <p class="error">{{ $error }}</p>
            @endforeach
        </div>
        {{ Form::hidden("picture", Input::old('picture', $artwork->picture), array("id" => "picture")) }}
        <div class="dark">
            {{Form::label('watermark',trans('artworks/form.label.watermark'))}}
            {{Form::radio('watermark',1,false)}} {{trans('forms.label.yes')}}
            {{Form::radio('watermark',0,true)}} {{trans('forms.label.no')}}
        </div>
        {{-- Submit --}}
        <div>
            {{ Form::hidden('picture_x', 0, ['id' => 'picture_x']) }}
            {{ Form::hidden('picture_y', 0, ['id' => 'picture_y']) }}
            {{ Form::hidden('picture_w', 0, ['id' => 'picture_w']) }}
            {{ Form::hidden('picture_h', 0, ['id' => 'picture_h']) }}
        </div>
        <div>
            <br>
            {{ Form::hidden('id', Input::old('id', $artwork->id)) }}
            {{ Form::button(trans('forms.button.accept'),['class'=>'button', 'type' => 'submit']) }}
        </div>
        {{ Form::close() }}
    </section>
</div>

<script language="Javascript">
    function getCoords(c)
    {
        var img = document.getElementById("main_picture");
        var scale = img.naturalWidth /200;
        $('#picture_x').val(c.x * scale);
        $('#picture_y').val(c.y * scale);
        $('#picture_w').val(c.w * scale);
        $('#picture_h').val(c.h * scale);

    };
</script>

<script language="JavaScript">
    var jcrop_api;
    // Update the picture of the form with the uploaded one
    function addPicture(name){
        // Empty the pictureError div
        document.getElementById("pictureError").innerHTML = "";
        if(!name.hasOwnProperty("error")){
            // We use the full path for the src of the image
            jQuery('#main_picture').attr("src", name);
            jQuery('#main_picture').show();
            // We only store the file name in the database
            var fileName = name.substring(name.lastIndexOf("/")+1);
            jQuery('#picture').val(fileName);
            if(jcrop_api!=null) {
                jcrop_api.destroy();
                jQuery('.jcrop-holder').remove();
            }
            jQuery(function($) {
                $('#main_picture').Jcrop({
                    onSelect: getCoords,
                    onChange: getCoords,
                    aspectRatio: 440 / 276,
                    setSelect:   [ 0, 0, 440, 276 ],
                    bgColor:     'black',
                    bgOpacity:   .4
                },function(){
                    jcrop_api = this;
                });
            });
        }
        else{
            // Create the custom error label
            var pictureErrorLabel = '<label id="pictureError" class="error hidden">'+name.error+'</label>';
            // Add the error label to the view
            jQuery('#pictureError').append(pictureErrorLabel);
        }
    }
</script>
@stop
