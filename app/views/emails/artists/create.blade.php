<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Bienbenido a Articonic</h2>

<div>
    <p>Enhorabuena, has sido elegido para formar parte de Articonic, la galería virtual de los nuevos talentos.</p>
    <p>Ahora podrás crear tu porpia galería virtual, difundir tu arte y mucho más</p>
    <p>Tu usuario: {{$email}}</p>
    <p>Tu contraseña: {{$password}}</p>
    <p>Puedes realizar login pulsando en el siguiente {{link_to_route('artist.login.get', 'enlace')}}</p>

    <p>Un saludo,</p>
    <P>El equipo de Articonic</P>
</div>
</body>
</html>