<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0">
    <script src="/js/vendor/jquery/jquery.min.js"></script>
    <title>Articonic</title>

    @yield('head')
</head>
<body>
{{-- Menu script --}}
@include('partials.header')

<div class="container">
    @yield('content')
</div>
@include('partials.footer')

{{-- Menu script --}}
@include('partials.menu')

</body>
</html>

