{{-- MENU --}}
@if(Auth::check())
<div class="row">
    <section class="12u">
        <ul class="nav">
            <li><a>{{link_to_route('artist.edit', trans('menu.profile'))}}</a></li>
            <li><a>{{link_to_route('artwork.dashboard', trans('menu.admin'))}}</a></li>
            <li><a>{{link_to_route('artwork.create', trans('menu.new'))}}</a></li>
        </ul>
    </section>
</div>
@endif
