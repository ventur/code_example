<!DOCTYPE HTML>
<html>
<head>
    <title>Articonic</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/vendor/jquery/jquery.min.js"></script>
    <script src="js/vendor/jquery/jquery.poptrox.min.js"></script>
    <script src="js/vendor/skeljs/skel.min.js"></script>
    <script>
        var error_messages = {
            EMAIL: {
                required: "{{trans('landing_page.contact.required')}}",
                email: "{{trans('landing_page.contact.email_format')}}"
            }
        };
    </script>
    <script src="js/articonic/landing_page/init.js"></script>
    <noscript>
        <link rel="stylesheet" href="styles/landing_page/skel-noscript.css" />
        <link rel="stylesheet" href="styles/landing_page/style.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="styles/landing_page/ie/v8.css" /><![endif]-->

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-49998449-1', 'esy.es');
        ga('send', 'pageview');

    </script>
    <!-- Google Analytics -->
</head>

<body>
<!-- Header -->
<header id="header">

    <!-- Logo -->
    <h1 id="logo"><a href="#"><img src="/images/articonic.png">Articonic</a></h1>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="#intro">@lang('landing_page.titles.menu1')</a></li>
            <li><a href="#one">@lang('landing_page.titles.menu2')</a></li>
            <li><a href="#two">@lang('landing_page.titles.menu3')</a></li>
            <li><a href="#work">@lang('landing_page.titles.menu4')</a></li>
            <li><a href="#contact">@lang('landing_page.titles.menu5')</a></li>
        </ul>
    </nav>

    <div id="lang">
        <a href="/es/{{Request::path()}}">es</a><a href="/en/{{Request::path()}}">en</a>
    </div>

</header>

<!-- Intro -->
<section id="intro" class="main style1 dark fullscreen">
    <div class="content container small">
        <header>
            <h2>@lang('landing_page.menu1.header')</h2>
        </header>
        <p>@lang('landing_page.menu1.text1')</p>
        <p>@lang('landing_page.menu1.text2')</p>
        <footer>
            <a href="#one" class="button style2 down">@lang('pagination.more')</a>
        </footer>
    </div>
</section>

<!-- One -->
<section id="one" class="main style2 right dark fullscreen">
    <div class="content box style2">
        <header>
            <h2>@lang('landing_page.menu2.header')</h2>
        </header>
        <p>@lang('landing_page.menu2.text1')</p>
    </div>
    <a href="#two" class="button style2 down anchored">@lang('pagination.next')</a>
</section>

<!-- Two -->
<section id="two" class="main style2 left dark fullscreen">
    <div class="content box style2">
        <header>
            <h2>@lang('landing_page.menu3.header')</h2>
        </header>
        <p>@lang('landing_page.menu3.text1')</p>
    </div>
    <a href="#work" class="button style2 down anchored">@lang('pagination.next')</a>
</section>

<!-- Work -->
<section id="work" class="main style3 primary">
    <div class="content container">
        <header>
            <h2>@lang('landing_page.titles.menu4')</h2>
            <p>@lang('landing_page.menu4.text1')</p>
            <p>@lang('landing_page.menu4.text2')</p>
        </header>

        <!--
             Lightbox Gallery

             Powered by poptrox. Full docs here: https://github.com/n33/jquery.poptrox
        -->
        <div class="container small gallery">
            <div class="row flush images">
                <div class="6u"><a href="images/landing_page/fulls/blanca-1.jpg" class="image full l"><img src="images/landing_page/thumbs/blanca-1.png" title="Blanca González" alt="" /></a></div>
                <div class="6u"><a href="images/landing_page/fulls/maria-1.png" class="image full r"><img src="images/landing_page/thumbs/maria-1.png" title="María Alcaide" alt="" /></a></div>
            </div>
            <div class="row flush images">
                <div class="6u"><a href="images/landing_page/fulls/maria-2.png" class="image full r"><img src="images/landing_page/thumbs/maria-2.png" title="María Alcaide" alt="" /></a></div>
                <div class="6u"><a href="images/landing_page/fulls/blanca-2.jpg" class="image full l"><img src="images/landing_page/thumbs/blanca-2.png" title="Blanca González" alt="" /></a></div>
            </div>
            <div class="row flush images">
                <div class="6u"><a href="images/landing_page/fulls/blanca-3.jpg" class="image full l"><img src="images/landing_page/thumbs/blanca-3.png" title="Blanca González" alt="" /></a></div>
                <div class="6u"><a href="images/landing_page/fulls/maria-3.jpg" class="image full r"><img src="images/landing_page/thumbs/maria-3.png" title="María Alcaide" alt="" /></a></div>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact" class="main style3 secondary">
    <div class="content container">
        <header>
            <h2>@lang('landing_page.menu5.header')</h2>
            <p>@lang('landing_page.menu5.text1')</p>
            <p>@lang('landing_page.menu5.text2')</p>
        </header>
        <div id="mc_embed_signup" class="box container small">

            <form action="http://esy.us8.list-manage.com/subscribe/post?u=36ba8536969ec5fb65417bc23&amp;id=54576be729"
                  method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="row half">
                    <div class="12u mc-field-group">
                        <input type="text" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="{{trans('landing_page.contact.email')}}" required/>
                    </div>
                </div>
                <div id="mce-responses" class="row half">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
                <div class="row half">
                    <div class="6u"><input type="text" name="FNAME" id="mce-FNAME" placeholder="{{trans('landing_page.contact.name')}}" /></div>
                    <div class="6u"><input type="text" name="LNAME" id="mce-LNAME" placeholder="{{trans('landing_page.contact.surname')}}" /></div>
                </div>
                <div class="row half">
                    <div class="12u"><textarea id="mce-MESSAGE" name="MESSAGE" placeholder="{{trans('landing_page.contact.message')}}" rows="6"></textarea></div>
                </div>

                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div class="row half" style="position: absolute; left: -5000px;">
                    <input type="text" name="b_36ba8536969ec5fb65417bc23_54576be729" value="">
                </div>

                <div class="row">
                    <div class="12u">
                        <ul class="actions">
                            <li><input type="submit" class="button" value="{{trans('landing_page.contact.submit')}}" /></li>
                        </ul>
                    </div>
                </div>
            </form>

        </div>
    </div>
</section>

<!-- Footer -->
<footer id="footer">

    <!--
         Social Icons

         Use anything from here: http://fortawesome.github.io/Font-Awesome/cheatsheet/)
    -->
    <ul class="actions">
        <li><a href="#" class="fa solo fa-twitter"><span>Twitter</span></a></li>
        <li><a href="#" class="fa solo fa-facebook"><span>Facebook</span></a></li>
        <li><a href="#" class="fa solo fa-google-plus"><span>Google+</span></a></li>
<!--        <li><a href="#" class="fa solo fa-dribbble"><span>Dribbble</span></a></li>-->
<!--        <li><a href="#" class="fa solo fa-pinterest"><span>Pinterest</span></a></li>-->
<!--        <li><a href="#" class="fa solo fa-instagram"><span>Instagram</span></a></li>-->
    </ul>

    <!-- Menu -->
    <ul class="menu">
<!--        <li>&copy; Untitled</li>-->
        <li>Design: <a href="http://html5up.net/">HTML5 UP</a></li>
    </ul>

</footer>

</body>
</html>