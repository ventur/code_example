{{-- Piwik snippet --}}
<script type="text/javascript">
    var _paq = _paq || [];
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function () {
        var u=(("https:" == document.location.protocol) ? "https" : "http") + "://analytics.mobiloptim.com/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', {{{ $piwikId or 1 }}}]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript>
    <p>
        <img src="https://analytics.mobiloptim.com/piwik.php?idsite={{{ $piwikId or 1 }}}" style="border:0;" alt="" />
    </p>
</noscript>
