<!-- Footer -->
<footer id="footer">
    <ul class="actions" style="text-align: right">
        <li><a href="https://www.facebook.com/pages/Articonic/732714926750218" target="_blank" class="fa solo fa-facebook"><span>Facebook</span></a></li>
        <li><a href="https://plus.google.com/109894616580551028312" target="_blank" class="fa solo fa-google-plus"><span>Google+</span></a></li>
        <li><a href="{{Request::url()}}?lang=en">EN</a> / <a href="{{Request::url()}}?lang=es">ES</a></li>
    </ul>
</footer>