<script type="application/javascript">

$( document ).ready(function() {
        $('#nav').prepend('<div id="menu-button"><img src="/images/icons/menu.svg"></div>');

        $('#nav #menu-button').on('click', function(){
            var menu = $(this).next('ul');
            if (menu.hasClass('open')) {
                menu.removeClass('open');
            }
            else {
                menu.addClass('open');
            }
        });

        $('.sub-menu').on('click', function(){
            var menu = $(this).next('ul');
            if (menu.hasClass('open')) {
                menu.removeClass('open');
            }
            else {
                menu.addClass('open');
            }
        });
    } )( jQuery );
</script>