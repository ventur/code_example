{{-- Header --}}
<header id="header">
    <div id="logo"><a href="/index"><img src="/images/logo.png"><p>Articonic</p></a></div>
    <div id="nav">
        <div id="menu-button"><img src="/images/icons/menu.svg"></div>
        <ul>
            <li>{{link_to_route('index',trans('menu.index'))}}</li>
            <li>{{link_to_route('artist.index', trans('menu.artist'))}}</li>
            <li>{{link_to_route('artwork.index', trans('menu.artwork'))}}</li>
            <li>{{link_to_route('contact.get', trans('menu.contact'))}}</li>
            @if(Auth::check())
                <li><a href="#">{{link_to_route('artist.logout',trans('menu.logout'))}}</a></li>
            @else
                <li><a href="#">{{link_to_route('artist.login.get', trans('menu.login'))}}</a></li>
            @endif
    </div>
</header>