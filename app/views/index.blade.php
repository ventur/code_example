<!doctype html>
<html lang="en" ng-app="articonicApp">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content='<?php echo csrf_token(); ?>'>
    <title>Articonic</title>

    <link rel="stylesheet" type="text/css" href="styles/css/layout.css">
    <link rel="stylesheet" type="text/css" href="vendor/normalize-scss/normalize.css">
</head>
<body>
    <header id="header" navigation-bar></header>

    <div id="view-content" ng-view ng-cloak></div>

    <section-footer></section-footer>

    <script src="vendor/jquery/dist/jquery.js"></script>
    <script src="vendor/angular/angular.min.js"></script>
    <script src="vendor/angular-route/angular-route.min.js"></script>
    <script src="vendor/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="vendor/angular-translate/angular-translate.min.js"></script>
    <script src="vendor/angular-translate-loader-url/angular-translate-loader-url.min.js"></script>
    <script src="vendor/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js"></script>
    <script src="vendor/angular-translate-loader-partial/angular-translate-loader-partial.min.js"></script>

    <script src="js/app.js"></script>
    <script src="js/directives/partialDirectives.js"></script>
    <script src="js/controllers/homeController.js"></script>
    <script src="js/controllers/artworksController.js"></script>
    <script src="js/controllers/artworkController.js"></script>
    <script src="js/controllers/artistsController.js"></script>
    <script src="js/controllers/galleryController.js"></script>
    <script src="js/controllers/contactController.js"></script>
    <script src="js/controllers/loginController.js"></script>
</body>
</html>