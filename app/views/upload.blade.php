<!DOCTYPE html>
<html lang="" class="no-js">
<body>
{{ Form::open([
'route' => ['picture.upload.post'],
'method' => 'post',
'files' => true,
'id' => 'upload'
]) }}
<div class = "browseImage" style="color: #ffffff;">
    {{ Form::file("picture_single", array("id" => "picture_input", "accept" => "image/*"))}}
    {{ Form::button(trans("forms.label.picture"),array("class" => "uploadButton", "id" => "browse-click", "style"=>"display:none")) }}
</div>
{{ Form::close() }}

<!-- JavaScript Includes -->
<script src="/js/vendor/jquery/jquery.min.js"></script>

<!-- jQuery File Upload Dependencies -->
<script src="/js/vendor/fileupload/jquery.ui.widget.js"></script>
<script src="/js/vendor/fileupload/jquery.iframe-transport.js"></script>
<script src="/js/vendor/fileupload/jquery.fileupload.js"></script>

<!-- Our main JS file -->
<script src="/js/vendor/fileupload/script.js"></script>
</body>
</html>
