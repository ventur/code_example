<?php

use Articonic\Repositories\ArtistRepositoryInterface;
use Articonic\Repositories\ArtworkRepositoryInterface;
use Illuminate\Support\Str;

class AuthController extends BaseController
{

    protected $artists;
    protected $artworks;

    /**
     * Class constructor.
     *
     * @param ArtistRepositoryInterface  $artists
     * @param ArtworkRepositoryInterface $artworks
     */
    public function __construct(ArtistRepositoryInterface $artists, ArtworkRepositoryInterface $artworks)
    {
        parent::__construct();

        $this->artists  = $artists;
        $this->artworks = $artworks;
    }

    /************************************************
     * NEW API FOR ANGULARJS
     ***********************************************/

    /**
     * Login the user in the platform
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function artistLogin()
    {
        //Validate the form
        $form = $this->artists->getLoginForm();

        if (!$form->isValid()) {
            return $this->redirectBack(['errors' => $form->getErrors()]);
        }

        //Get the credentials
        $credentials['email']    = $form->getEmail();
        $credentials['password'] = $form->getPassword();

        //Login
        if (Auth::attempt($credentials)) {
            return $this->responseJson(['user' => Auth::user()]);
        } else {
            return $this->responseCode(401);
        }
    }

    /**
     * Artist logout
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function artistLogout()
    {
        Auth::logout();
        return $this->responseSuccess(true);
    }


    /************************************************
     * ROUTES FOR OLD LARAVEL VIEWS
     ***********************************************/

    /**
     * Create the artist and send the credentials
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postArtistSignup()
    {
        //Validate the form
        $form = $this->artists->getArtistSignupForm();
        if (!$form->isValid()) {
            return $this->redirectBack(['errors' => $form->getErrors()]);
        }

        //Create the artist
        $email = $form->getEmail();
        $password = Str::random(8);
        $this->artists->createArtist($email,$password);

        //Send the email
        Mail::send('emails.artists.create',['email' => $email, 'password' => $password], function($message) use ($email)
        {
            $message->to($email)->subject('Tu cuenta ha sido creada');
        });

        return $this->redirectRoute('artist.login.get');
    }

}
