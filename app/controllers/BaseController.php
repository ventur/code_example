<?php

class BaseController extends Controller
{
    /**
     * Create a new BaseController instance.
     */
    public function __construct()
    {
        $this->beforeFilter('csrf_header', ['on' => 'post']);
    }

    /**
     * Setup the layout used by the controller.
     */
    protected function setupLayout()
    {
        if (! is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     * Set the specified view as content on the layout.
     *
     * @param  string  $path
     * @param  array   $data
     */
    protected function view($path, $data = [])
    {
        $this->layout->content = View::make($path, $data);
    }

    /**
     * Return the specified view.
     *
     * @param  string  $path
     * @param  array   $data
     *
     * @return Illuminate\View\View
     */
    protected function viewMake($path, $data = [])
    {
        return View::make($path, $data);
    }

    /**
     * Redirect to the specified named route.
     *
     * @param  string  $route
     * @param  array   $params
     * @param  array   $data
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectRoute($route, $params = [], $data = [])
    {
        return Redirect::route($route, $params)->with($data);
    }

    /**
     * Redirect back with old input and the specified data.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectBack($data = [])
    {
        return Redirect::back()->withInput()->with($data);
    }

    /**
     * Redirect a logged in user to the previously intended url.
     *
     * @param  mixed  $default
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectIntended($default = null)
    {
        return Redirect::intended($default);
    }

    /**
     * Return a JSON response indicating success.
     *
     * @param  bool  $success
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseSuccess($success)
    {
        return Response::json(['success' => $success]);
    }

    /**
     * Return a JSON response
     *
     * @param array $data
     *
     * @return Illuminate\Http\JsonResponse
     */
    protected function responseJson($data)
    {
        return Response::json($data);
    }

    /**
     * Return a empty response with status code
     *
     * @param integer $code
     *
     * @return Illuminate\Http\JsonResponse
     */
    protected function responseCode($code)
    {
        return Response::json([],$code);
    }

}
