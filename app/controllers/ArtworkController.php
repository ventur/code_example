<?php

use Articonic\Repositories\ArtistRepositoryInterface;
use Articonic\Repositories\ArtworkRepositoryInterface;
use PHPImageWorkshop\ImageWorkshop;

class ArtworkController extends BaseController {

    protected $artists;
    protected $artworks;
    protected $scale = 200;

    /**
     * Class constructor.
     *
     */
    public function __construct(ArtistRepositoryInterface $artists, ArtworkRepositoryInterface $artworks)
    {
        parent::__construct();

        $this->artists = $artists;
        $this->artworks = $artworks;
    }

    /************************************************
     * NEW API FOR ANGULARJS
     ***********************************************/

    /**
     * Get all artworks with pagination
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $limit = Input::has('limit') ? Input::get('limit') : 12;
        $artworks = $this->artworks->getPaginate($limit);

        return $this->responseJson($artworks);
    }

    /**
     * Get the latest artworks
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function getLatest() {
        $limit = Input::has('limit') ? Input::get('limit') : 12;
        $models = $this->artworks->getLatestArtworks($limit);
        $artworks = array();
        foreach($models as $model) {
            $artworks[] = [
                'id' => $model->id,
                'picture' => $model->picture,
            ];
        }
        return $this->responseJson($artworks);
    }

    /**
     * Get the artist gallery
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function gallery($slug) {
        $limit = Input::has('limit') ? Input::get('limit') : 12;
        $artist   = Artist::whereSlug($slug)->first();
        $artworks = $this->artworks->getGallery($artist->id,$limit);

        return $this->responseJson($artworks);
    }

    /**
     * Get the artwork information
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function profile($id) {
        return $this->artworks->getById($id);
    }

    /************************************************
     * ROUTES FOR OLD LARAVEL VIEWS
     ***********************************************/

    /**
     * Get create artwork form
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return $this->viewMake('artworks.create');
    }

    /**
     * Save a new artwork
     *
     * @return Illuminate\View\View
     */
    public function store()
    {
        //Validate the form
        $form = $this->artworks->getArtworkForm();

        if (!$form->isValid()) {
            return $this->redirectBack(['errors' => $form->getErrors()]);
        }

        $artwork = $this->artworks->createArtwork($form->getInputData());
        $this->generatePictures($form->getPicture(), $form->getPictureCoord(),$form->getWatermark(), $artwork->id);

        return $this->redirectRoute('artwork.show',['id'=>$artwork->id]);
    }

    /**
     * Get the artwork dashboard of the user
     *
     * @return Illuminate\View\View
     */
    public function dashboard() {
        $artist = Auth::user();
        $artworks = $this->artworks->findOwnArtworks($artist->id);
        $categories = $this->artworks->getCategories();

        return $this->viewMake('artworks.dashboard', compact('categories', 'artworks'));
    }

    /**
     * Get edit artwork form
     *
     * @param integer $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $artwork = $this->artworks->find($id);

        if (Auth::user()->id == $artwork->artist_id) {

            return $this->viewMake('artworks.edit', compact('artwork'));
        }

        return $this->redirectRoute('artwork.show',$artwork->id);
    }

    /**
     * Update an existing artwork
     *
     * @return Illuminate\View\View
     */
    public function update() {

        //Validate the form
        $form = $this->artworks->getArtworkForm();

        if (!$form->isValid()) {
            return $this->redirectBack(['errors' => $form->getErrors()]);
        }

        $artwork = $this->artworks->find($form->getId());

        if (Auth::user()->id == $artwork->artist_id) {
            $artwork = $this->artworks->update($artwork, $form->getInputData());
        }

        return $this->redirectRoute('artwork.show',['id'=>$artwork->id]);
    }

    /**
     * Get upload picture iframe
     *
     * @return Illuminate\View\View
     */
    public function getPictureUpload()
    {
        return $this->viewMake('upload');
    }

    /**
     * Upload a picture
     *
     * @return string|array path to the uploaded file
     */
    public function postPictureUpload()
    {
        $data =  Input::all();
        $validator = Validator::make(
            $data,
            array(
                'picture' => 'image|mimes:jpeg,gif,png|max:8000',
            ));
        if ($validator->fails()) {
            return ["error" => $validator->errors()->getMessages()["picture"][0]];
        }

        $picture = $data["picture"];
        $originalName = $picture->getClientOriginalName();
        $extension = $picture->getClientOriginalExtension();
        $name = str_replace('.'.$extension,'',$originalName);
        $path = 'upload/tmp/';

        $upload_success = $picture->move($path,$originalName);
        $image = ImageWorkshop::initFromPath(public_path($path.$originalName));
        if(strtolower($extension)!='jpeg') {
            $image->save(public_path($path),$name.'.jpeg',false,'#ffffff',90);
            unlink(public_path($path.$originalName));
        }
        //Medium
        $image->resizeInPixel(null, 1024, true);
        $image->save(public_path($path), 'medium-'.$name.'.jpeg', true, null, 90);

        if ($upload_success) {
            return '/'.$path.$name.'.jpeg';
        } else {
            return ["error" => "Error uploading the file"];
        }
    }

    /**
     * Generate the three picture sizes
     *
     * @param $picture
     * @param $coord
     * @param $watermark
     * @param $artworkId
     *
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     */
    protected function generatePictures($picture, $coord, $watermark, $artworkId)
    {
        ini_set('max_execution_time', 123456);
        Log::error(date('Y-m-d H:i:s').': Starting');

        //Move to the correct folder
        mkdir(public_path('upload/artworks/' . $artworkId . '/large/'),0777,true);
        mkdir(public_path('upload/artworks/' . $artworkId . '/medium/'),0777,true);
        rename(public_path('upload/tmp/'.$picture),public_path('upload/artworks/' . $artworkId . '/large/0.jpeg'));
        rename(public_path('upload/tmp/medium-'.$picture),public_path('upload/artworks/' . $artworkId . '/medium/0.jpeg'));

        Log::error(date('Y-m-d H:i:s').': Generating medium');
        //medium
        $image = ImageWorkshop::initFromPath(public_path('upload/artworks/' . $artworkId . '/medium/0.jpeg'));
        if ($watermark) {
            $watermarkLayer = ImageWorkshop::initFromPath(public_path('images/watermark_medium.png'));
            $image->addLayerOnTop($watermarkLayer, 0, 0, 'MM');
        }
        $image->save(public_path('upload/artworks/' . $artworkId . '/medium'), '0.jpeg', false, null, 100);
        Log::error(date('Y-m-d H:i:s').': Generating thumb');

        //thumb
        $image = ImageWorkshop::initFromPath(public_path('upload/artworks/' . $artworkId . '/large/0.jpeg'));
        $image->cropInPixel($coord['w'],$coord['h'],$coord['x'],$coord['y']);
        $image->resizeInPixel(440, null, true);
        //$image->cropInPixel(440, 276, 0, 0, 'MM');
        if ($watermark) {
            $watermarkLayer = ImageWorkshop::initFromPath(public_path('images/watermark_thumb.png'));
            $image->addLayerOnTop($watermarkLayer, 0, 0, 'MM');
        }
        $image->save(public_path('upload/artworks/' . $artworkId . '/thumb'), '0.jpeg', true, null, 100);
        Log::error(date('Y-m-d H:i:s').': Generating large');

        //watermark in large
        if ($watermark) {
            $image          = ImageWorkshop::initFromPath(public_path('upload/artworks/' . $artworkId . '/large/0.jpeg'));
            $watermarkLayer = ImageWorkshop::initFromPath(public_path('images/watermark_medium.png'));
//            if($image->getHeight()<$watermarkLayer->getHeight()) {
//                $watermarkLayer->resizeInPixel(null, $image->getHeight() * 0.8, true);
//            }
            $image->addLayerOnTop($watermarkLayer, 0, 0, 'MM');
            $image->save(public_path('upload/artworks/' . $artworkId . '/large'), '0.jpeg', true, null, 100);
        }
        Log::error(date('Y-m-d H:i:s').': Finishing');
    }
} 