<?php

use Articonic\Repositories\ArtistRepositoryInterface;
use Articonic\Repositories\ArtworkRepositoryInterface;
use Articonic\Repositories\WebsiteRepositoryInterface;

class HomeController extends BaseController {

    protected $artists;
    protected $artworks;
    protected $website;

    /**
     * Class constructor.
     *
     * @param ArtistRepositoryInterface  $artists
     * @param ArtworkRepositoryInterface $artworks
     * @param WebsiteRepositoryInterface $website
     */
    public function __construct(ArtistRepositoryInterface $artists, ArtworkRepositoryInterface $artworks, WebsiteRepositoryInterface $website)
    {
        parent::__construct();

        $this->artists = $artists;
        $this->artworks = $artworks;
        $this->website = $website;
    }

    /**
     * Get the main page
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return $this->viewMake('index');
    }

    /**
     * Get the contact message and send it by email
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postContact()
    {
        //Validate the form
        $form = $this->website->getContactForm();
        if (!$form->isValid()) {
            return $this->responseJson(['errors' => $form->getErrors()]);
        }

        //Send the email
        Mail::send('emails.contact',$form->getInputData(), function($message)
        {
            $message->to('info@articonic.com')->subject('Un usuario ha enviado un mensaje');
        });

        return $this->responseSuccess(true);
    }

}