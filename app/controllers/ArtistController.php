<?php

use Articonic\Repositories\ArtistRepositoryInterface;
use Articonic\Repositories\ArtworkRepositoryInterface;
use PHPImageWorkshop\ImageWorkshop;

class ArtistController extends BaseController
{

    protected $artists;
    protected $artworks;

    /**
     * Class constructor.
     *
     */
    public function __construct(ArtistRepositoryInterface $artists, ArtworkRepositoryInterface $artworks)
    {
        parent::__construct();

        $this->artists  = $artists;
        $this->artworks = $artworks;
    }

    /************************************************
     * NEW API FOR ANGULARJS
     ***********************************************/

    /**
     * Get all artists with pagination
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $limit = Input::has('limit') ? Input::get('limit') : 10;
        $artist = $this->artists->getPaginate($limit);
        return $this->responseJson($artist);
    }

    /**
     * Get the latest artists
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function getLatest()
    {
        $limit = Input::has('limit') ? Input::get('limit') : 12;
        $artists = $this->artists->getLatestArtists($limit);
        return $this->responseJson($artists);
    }

    /**
     * Get an artist preview
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function preview($slug) {
        $artist   = Artist::whereSlug($slug)->first();
        $artworks = Artwork::whereArtistId($artist->id)->take(4)->get();

        return $this->responseJson(['profile' => $artist, 'artworks' => $artworks]);
    }

    /**
     * Get an artist profile
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function profile($slug) {
        $artist   = Artist::whereSlug($slug)->first();

        return $this->responseJson($artist);
    }


    /************************************************
     * ROUTES FOR OLD LARAVEL VIEWS
     ***********************************************/

    /**
     * Get initialize artist form
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return $this->viewMake('artists.create');
    }

    /**
     * Get initialize artist form
     *
     * @return Illuminate\View\View
     */
    public function store()
    {
        //Validate the form
        $form = $this->artists->getArtistForm();
        if (!$form->isValid()) {
            return $this->redirectBack(['errors' => $form->getErrors()]);
        }

        //Update the user
        $artist = Auth::user();
        $this->artists->initialize($artist, $form->getInputData());
        $this->generatePictures($form->getPicture(),$artist->id);

        return $this->redirectRoute('artwork.create');
    }

    /**
     * Get edit artist form
     *
     * @return Illuminate\View\View
     */
    public function edit()
    {
        $artist = Auth::user();
        $birthday = explode('-',$artist->date_of_birth);
        if (Session::has('tab')) {
            $tab = Session::get('tab');
        } else {
            $tab = 1;
        }
        return $this->viewMake('artists.edit',compact('artist','birthday','tab'));
    }

    /**
     * Update profile data of artist
     *
     * @return Response
     */
    public function update()
    {
        //Validate the form
        $form = $this->artists->getArtistUpdateForm();
        if (!$form->isValid()) {
            return $this->redirectBack(['errors' => $form->getErrors()]);
        }

        //Update the user
        $artist = Auth::user();
        $this->artists->updateProfileData($artist, $form->getInputData());
        if ($form->getPicture()) {
            $this->generatePictures($form->getPicture(),$artist->id);
        }

        return $this->redirectRoute('artist.show',[$artist->slug]);
    }

    /**
     * Change artist password
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChangePassword()
    {
        //Validate the form
        $form = $this->artists->getChangePasswordForm();
        Session::flash('tab', 2);

        if (!$form->isValid()) {
            return $this->redirectBack(['errors' => $form->getErrors()]);
        }

        if (!$form->validateCurrentPassword()) {
            return $this->redirectBack()->with('password_mismatch', 'Tu contraseña actual es incorrecta');
        }

        //Update the password
        $artist = Auth::user();
        $artist->setPassword($form->getNewPassword());
        $artist->save();

        return $this->redirectBack()->with('password_changed', 'Tu contraseña se ha actualizado correctamente');
    }

    /**
     * Get the user page
     *
     * @param string $slug
     *
     * @return Illuminate\View\View
     */
    public function show($slug)
    {
        $artist   = Artist::whereSlug($slug)->first();
        $artworks = Artwork::whereArtistId($artist->id)->get();
        $categories = $this->artworks->getCategories();

        return $this->viewMake('artists.show', compact('artist', 'artworks', 'categories'));
    }

    /**
     * Get the user gallery
     *
     * @param string $slug
     *
     * @return Illuminate\View\View
     */
    public function gallery($slug)
    {
        $artist   = Artist::whereSlug($slug)->first();
        $artworks = Artwork::whereArtistId($artist->id)->get();
        $categories = $this->artworks->getCategories();

        return $this->viewMake('artists.gallery', compact('artist', 'artworks', 'categories'));
    }

    /**
     * Convert to jpeg and generate the thumbnails pictures
     *
     * @param $picture
     * @param $id
     *
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     */
    protected function generatePictures($picture, $id)
    {
        //normal
        $originalName = $picture->getClientOriginalName();
        $extension = $picture->getClientOriginalExtension();

        $picture->move(public_path('upload/artists'), $originalName);
        $image = ImageWorkshop::initFromPath(public_path('upload/artists/' . $originalName));
        $image->save(public_path('upload/artists/'), $id . '.jpeg', '#ffffff', null, 90);
        if(strtolower($extension)!='jpeg') {
            unlink(public_path('upload/artists/' . $picture->getClientOriginalName()));
        }
        //thumb
        $image = ImageWorkshop::initFromPath(public_path('upload/artists/' . $id . '.jpeg'));
        $image->resizeInPixel(440, null, true);
        $image->cropInPixel(440, 276, 0, 0, 'MM');
        $image->save(public_path('upload/artists/thumbs'), $id . '.jpeg', '#ffffff', null, 90);
    }

}