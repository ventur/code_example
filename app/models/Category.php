<?php

class Category extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

    /**
     * Auto timestamp
     * @var bool
     */
    public $timestamps = true;

    /**
     * Mass asignment attributes
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get Artworks. One to Many Relationship
     *
     * @return mixed
     */
    public function artworks()
    {
        return $this->hasMany('Artwork');
    }

}