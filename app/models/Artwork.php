<?php

class Artwork extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'artworks';

    /**
     * Auto timestamp
     * @var bool
     */
    public $timestamps = true;

    /**
     * Mass asignment attributes
     * @var array
     */
    protected $fillable = ['name', 'description', 'category_id', 'artist_id', 'picture'];

    protected $hidden = ['created_at','updated_at','revised'];

    /**
     * Get Artist. One to One Relationship
     *
     * @return mixed
     */
    public function artist()
    {
        return $this->belongsTo('Artist');
    }

    /**
     * Get Category. One to One Relationship
     *
     * @return mixed
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }

}