# Documentation about how to work with this file is available at
# http://compass-style.org/help/tutorials/configuration-reference/

# Set the environment mode.
environment = :development

# Location of the project's resources.
sass_dir        = "assets/sass"
css_dir         = "public/styles/css"
images_dir      = "public/styles/img"
javascripts_dir = "public/js"

# Can be one of :expanded, :nested, :compact or :compressed
output_style = :expanded
