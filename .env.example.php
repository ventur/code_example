<?php

return [

    // App Encryption Key
    'APP_KEY'                     => 'app_encryption_key',

    // Main database configuration variables
    'MAIN_DATABASE_HOST'          => 'host',
    'MAIN_DATABASE_NAME'          => 'database_name',
    'MAIN_DATABASE_USERNAME'      => 'username',
    'MAIN_DATABASE_PASSWORD'      => 'password',

    // Migration database configuration variables
    'MIGRATION_DATABASE_HOST'     => 'host',
    'MIGRATION_DATABASE_NAME'     => 'database_name',
    'MIGRATION_DATABASE_USERNAME' => 'username',
    'MIGRATION_DATABASE_PASSWORD' => 'password',

    // SMTP Server
    'MAIL_SMTP_HOST'              => 'SMTP host',
    'MAIL_ADDRESS'                => 'From email addres',
    'MAIL_USERNAME'               => 'username',
    'MAIL_PASSWORD'               => 'password',

];
