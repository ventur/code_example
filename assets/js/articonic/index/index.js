
//TODO: improve to use more tabs it is necessary. EX: search by *-tab to hide all
$( document ).ready(function() {
    var tabs = $(".tab");
    // Click Handler
    tabs.click(function( e ) {
        e.preventDefault();
        if ( $(this).attr('id') == "artist") {
            $('#artist-square').show();
            $(this).addClass('active');
            $('#artwork-square').hide();
            $('#artwork').removeClass('active');
        } else {
            $('#artist-square').hide();
            $('#artist').removeClass('active');
            $('#artwork-square').show();
            $(this).addClass('active');
        }

    });
});

function toggleSection(section) {
    if(jQuery(section)[0].id == 'artwork-title') {
        jQuery('#artwork-content').toggle();
    }
    if(jQuery(section)[0].id == 'artist-title') {
        jQuery('#artist-content').toggle();

    }
}