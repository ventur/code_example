// Images-related tasks
// ====================

    // Load gulp and the tasks config
var gulp         = require('gulp'),
    config       = require('./config.js'),

    // Filter files using globbing
    gulpFilter   = require('gulp-filter'),

    // Optimize the size of SVG files
    svgmin       = require('gulp-svgmin');


// Task definition
// ---------------

// The images task optimizes SVG files and copies
// all of the images to the public assets directory.
gulp.task('images', ['clean-images'], function() {

    var source = [
            config.paths.src.img+'/**',
            config.paths.src.icons+'/**'
        ];

    var svgFilter = gulpFilter('**/*.svg');

    return gulp.src(source)
        // Optimize SVG files
        .pipe(svgFilter)
        .pipe(svgmin())
        .pipe(svgFilter.restore())
        // Export all of the images to the public assets directory
        .pipe(gulp.dest(config.paths.dist.img));
});
