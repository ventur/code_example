// Styles-related tasks
// ====================

    // Load gulp and the tasks config
var gulp         = require('gulp'),
    config       = require('./config.js'),

    // SASS compilation using Compass and the SASS Ruby gem
    compass      = require('gulp-compass'),

    // Add vendor prefixes to CSS using values from the Can I Use website
    autoprefixer = require('gulp-autoprefixer');


// Process styles
// --------------

// The styles task compiles SASS files to CSS and then runs
// Autoprefixer to add vendor prefixes according to data
// sets from the Can I Use reference. Once it’s done, CSS
// files are output to the public assets directory.
gulp.task('styles', ['clean-styles'], function() {

    return gulp.src(config.paths.src.sass+'/*.scss')
        .pipe(compass({
            config_file: './config.rb',
            css:         config.paths.dist.css,
            sass:        config.paths.src.sass
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest(config.paths.dist.css));
});
