// Icon font generation
// ====================

    // Load gulp and the tasks config
var gulp         = require('gulp'),
    config       = require('./config.js'),

    // Generate icon fonts from SVG files
    iconfont     = require('gulp-iconfont'),

    // Create SASS files for the icon fonts
    iconfontCss  = require('gulp-iconfont-css');


// Settings
// --------

    // Name of the icon font
var fontName = 'icons',

    // Settings to generate the SASS file related to the icon font
    iconFontCssSettings = {

        fontName:   fontName,
        // Set the template
        path:       'assets/sass/shared/icons_scss_template',
        // Path were the file should be saved, relative to the gulp.dest() path
        targetPath: '../../../../assets/sass/shared/typography/_icon-font.scss',
        // Directory of font files, relative to the final CSS file
        fontPath:   '../fonts/icons/'
    };


// Generate the icon font
// ----------------------

// This task generates icon font files from a set of SVG
// files and exports them to the public assets directory.
// It then generates a SASS file containing rules for
// all of the icons and outputs it to the SASS directory.
gulp.task('iconfont', ['clean-fonts'], function() {

    // This task is a dependency of the ‘styles’
    // task because the SASS file it generates
    // needs to be created before SASS compilation.

    return gulp.src(config.paths.src.svgIcons+'/*.svg')
        .pipe(iconfontCss(iconFontCssSettings))
        .pipe(iconfont({fontName: fontName}))
        .pipe(gulp.dest(config.paths.dist.fonts+'/icons'));
});
