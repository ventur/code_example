// This ‘shortcut task’ builds the front end
// assets by calling a series of other tasks.
require('gulp').task('build-assets', ['fonts', 'styles', 'scripts', 'images']);
