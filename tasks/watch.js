// Watch configuration
// ===================

    // Load gulp and the tasks config
var gulp         = require('gulp'),
    config       = require('./config.js'),
    paths        = config.paths,

    // Live reload of stylesheets in browsers
    livereload   = require('gulp-livereload');


// Watch task definition
// ---------------------
gulp.task('watch', function() {

    // Watch SASS files
    gulp.watch(paths.src.sass+'/**/*.scss', ['styles']);

    // Watch JavaScript files
    gulp.watch(paths.src.js+'/**/*.js', ['scripts']);

    // Watch changes in the public CSS dir and reload browsers
    gulp.watch(paths.dist.css+'/*.css').on('change', function(file) {
        livereload().changed(file.path);
    });

});
