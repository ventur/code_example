// Cleaning tasks
// ==============

// These tasks remove generated assets from
// the public document root of the project.

    // Load gulp and the tasks config
var gulp         = require('gulp'),
    config       = require('./config.js'),

    // This plugin removes files and folders
    clean        = require('gulp-clean');


// Remove font files
// -----------------
gulp.task('clean-fonts', function() {
    return gulp.src(config.paths.dist.fonts, {read: false}).pipe(clean());
});


// Remove stylesheets
// ------------------
gulp.task('clean-styles', function() {
    return gulp.src(config.paths.dist.css, {read: false}).pipe(clean());
});


// Remove scripts
// --------------
gulp.task('clean-scripts', function() {
    return gulp.src(config.paths.dist.js, {read: false}).pipe(clean());
});


// Remove front end vendor packages
// --------------
gulp.task('clean-vendor', function() {
    return gulp.src(config.paths.dist.vendor, {read: false}).pipe(clean());
});


// Remove images
// -------------
gulp.task('clean-images', function() {
    return gulp.src(config.paths.dist.img, {read: false}).pipe(clean());
});


// Run all the ‘clean’ subtasks
// ----------------------------
gulp.task('clean', [
    'clean-fonts',
    'clean-styles',
    'clean-scripts',
    'clean-images'
]);
