// Fonts-related tasks
// ===================

    // Load gulp and the tasks config
var gulp         = require('gulp'),
    config       = require('./config.js'),
    paths        = config.paths;


// The fonts tasks copy font files
// to the public assets directory.
gulp.task('fonts', ['iconfont'], function() {
    return gulp.src(paths.src.fonts+'/**')
        .pipe(gulp.dest(paths.dist.fonts));
});
