// Scripts-related tasks
// =====================

    // Load gulp and the tasks config
var gulp         = require('gulp'),
    config       = require('./config.js'),

    // Concatenate files
    concat       = require('gulp-concat');


// This task concatenates JavaScript files and outputs
// a single bundle file in the public assets directory.
gulp.task('scripts', ['clean-scripts'], function() {

    return gulp.src(config.paths.src.js+'/*.js')
        .pipe(gulp.dest(config.paths.dist.js))
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest(config.paths.dist.js));
});
