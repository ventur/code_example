// Configuration of the gulp tasks
// ===============================

module.exports = {

    // Paths to source and production assets
    paths: {
        src: {
            fonts:    'assets/fonts',
            icons:    'assets/icons',
            img:      'assets/images',
            js:       'assets/javascript',
            sass:     'assets/sass',
            svgIcons: 'assets/icons/svg',
            vendor:   'assets/vendor'
        },
        dist: {
            css:   'public/styles/css',
            fonts: 'public/styles/fonts',
            img:   'public/styles/img',
            js:    'public/js',
            vendor: 'public/vendor'
        }
    }
};
