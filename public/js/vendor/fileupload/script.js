$(function(){

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({

        //Name of the parameter in POST
        paramName: 'picture',

        // This function is called when a file is added to the queue;
        // via the browse button
        add: function (e, data) {
            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
        },

        done : function(e,data){
            result = data.result;
            window.parent.addPicture(result);
        },

        fail:function(e, data){
            // Something has gone wrong!
            console.log(e);
        }

    });
});