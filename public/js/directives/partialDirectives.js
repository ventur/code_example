

angular.module('partialDirective', [])

    .directive('navigationBar', function () {
        return {
            restrict: 'A',
            templateUrl: 'views/partials/navigation-bar.html',
            controller: ['$scope' ,function ($scope) {
                var ctrl = this;
                if($(window).width() >= 1000) {
                    ctrl.menu = true;
                } else {
                    ctrl.menu = false;
                }

                $(window).on("resize.doResize", function (){
                        $scope.$apply(function(){
                            if($(window).width() >= 1000) {
                                ctrl.menu = true;
                            }
                        });
                    });

                $scope.$on("$destroy",function (){
                    //remove the handler added earlier
                    $(window).off("resize.doResize").off("load");
                });

                this.toggleMenu = function() {
                    ctrl.menu=!this.menu;
                };
                this.isMenu = function() {
                    return ctrl.menu;
                };

                this.submenu = null;
                this.openSubmenu = function(menu) {
                    if(this.submenu === menu) {
                        this.submenu = null;
                    } else {
                        this.submenu = menu;
                    }
                }
                this.isOpen = function(menu) {
                    return this.submenu === menu;
                }
            }],
            controllerAs: 'nav'
        };
    })
    .directive('sectionFooter', function () {
        return {
            restrict: 'E',
            templateUrl: 'views/partials/footer.html',
            controller: function () {
            },
            controllerAs: 'footer'
        };
    })

    .directive('pagination', ['Artwork', function (Artwork) {
        return {
            restrict: 'E',
            scope: {
                pagination: '=pagination',
                factory: '=factory'
            },
            templateUrl: 'views/partials/pagination.html',
            link: function (scope) {
                scope.nextPage = function () {
                    if (scope.current_page < scope.totalPages) {
                        scope.current_page++;
                    }
                };

                scope.prevPage = function () {
                    if (scope.current_page > 1) {
                        scope.current_page--;
                    }
                };

                scope.firstPage = function () {
                    scope.current_page = 1;
                };

                scope.lastPage = function () {
                    scope.current_page = scope.totalPages;
                };

                scope.setPage = function (page) {
                    scope.current_page = page;
                };

                var paginate = function (results, oldResults) {
                    if (oldResults === results) return;
                    scope.current_page = results.current_page;
                    scope.total = results.total;
                    scope.totalPages = results.last_page;
                    scope.pages = [];

                    for (var i = 1; i <= scope.totalPages; i++) {
                        scope.pages.push(i);
                    }
                };

                var pageChange = function (newPage, last_page) {
                    if (newPage == last_page) return;
                    scope.factory.page(newPage).success(function(data) {
                        angular.copy(data.data, scope.pagination.data);
                        scope.pagination.current_page = data.current_page;
                    });
                };

                scope.$watch('pagination', paginate);
                scope.$watch('current_page', pageChange);
            }
        }
    }])
    .directive('lazyStyle', function () {
        var loadedStyles = {};
        return {
            restrict: 'E',
            link: function (scope, element, attrs) {

                //add <lazy-style href="styles/css/home.css"/> in html

                attrs.$observe('href', function (value) {

                    var stylePath = value;

                    if (stylePath in loadedStyles) {
                        return;
                    }

                    if (document.createStyleSheet) {
                        document.createStyleSheet(stylePath); //IE
                    } else {
                        var link = document.createElement("link");
                        link.type = "text/css";
                        link.rel = "stylesheet";
                        link.href = stylePath;
                        document.getElementsByTagName("head")[0].appendChild(link);
                    }

                    loadedStyles[stylePath] = true;

                });
            }
        };
    })
    .directive('lazyJs', function () {
        var loadedJs = {};
        return {
            restrict: 'E',
            link: function (scope, element, attrs) {

                //add <lazy-js href="styles/css/home.css"/> in html

                attrs.$observe('src', function (value) {

                    var jsPath = value;

                    if (jsPath in loadedJs) {
                        return;
                    }

                    var link = document.createElement("script");
                    link.type = "application/javascript";
                    link.src = jsPath;
                    document.getElementsByTagName("head")[0].appendChild(link);

                    loadedJs[jsPath] = true;

                });
            }
        };
    })

    .directive('homeNews', ['Artwork','Artist', function(Artwork,Artist){
        return {
            restrict: 'E',
            templateUrl: 'views/partials/home-news.html',
            scope: {
                limit: '=limit'
            },
            link: function (scope) {
                scope.newArtists = {};
                scope.newArtworks = {};

                Artwork.latest(scope.limit)
                    .success( function(data) {
                        scope.newArtworks = data;
                    });
                Artist.latest(scope.limit)
                    .success( function(data) {
                        scope.newArtists = data;
                    });
            }
        }
    }]);