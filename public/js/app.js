(function () {
    var app = angular.module('articonicApp', [
        'ngRoute',
        'ngSanitize',
        'pascalprecht.translate',
        'partialDirective'
    ]);

    app.config(['$httpProvider', function($httpProvider){
        $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = $('meta[name=csrf-token]').attr('content');
    }])

    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                when('/', {
                    templateUrl: 'views/home.html',
                    controller: 'homeController',
                    controllerAs: 'home'
                }).
                when('/artists', {
                    templateUrl: 'views/artist/artists.html',
                    controller: 'artistsController',
                    controllerAs: 'artistsCtrl'
                }).
                when('/artworks', {
                    templateUrl: 'views/artwork/artworks.html',
                    controller: 'artworksController',
                    controllerAs: 'artworks'
                }).
                when('/artwork/:id', {
                    templateUrl: 'views/artwork/profile.html',
                    controller: 'artworkController',
                    controllerAs: 'artworkCtrl'
                }).
                when('/contact', {
                    templateUrl: 'views/contact.html',
                    controller: 'contactController',
                    controllerAs: 'contact'
                }).
                when('/login', {
                    templateUrl: 'views/login.html',
                    controller: 'loginController',
                    controllerAs: 'login'
                }).
                when('/:slug', {
                    templateUrl: 'views/artist/gallery.html',
                    controller: 'galleryController',
                    controllerAs: 'gallery'
                }).
                otherwise({
                    redirectTo: '/'
                });
    }]);

    app.config(['$translateProvider', function ($translateProvider) {
        $translateProvider
            //.useLoader('$translatePartialLoader', {
            //    urlTemplate: 'js/translations/{part}/{lang}.json'
            //})
            .useStaticFilesLoader({
                prefix: 'js/translations/locale-',
                suffix: '.json'
            })
            .preferredLanguage('es');
    }]);

    app.factory('Artwork', function($http) {
        return {
            profile: function(id) {
                return $http.get('/api/artwork/'+id+'/profile');
            },
            page: function(page) {
                return $http.get('/api/artwork/paginate?page='+page);
            },
            gallery: function(slug) {
                return $http.get('/api/artwork/gallery/'+slug);
            },
            latest: function (limit) {
                limit = limit != null ? '?limit=' + limit : '';
                return $http.get('/api/artwork/latest'+limit);
            }
        }
    });

    app.factory('Artist', function($http) {
        return {
            // get all the comments
            profile: function(slug) {
                return $http.get('/api/artist/'+slug+'/profile');
            },
            page: function(page) {
                return $http.get('/api/artist/paginate?page='+page);
            },
            latest: function (limit) {
                limit = limit != null ? '?limit=' + limit : '';
                return $http.get('/api/artist/latest'+limit);
            },
            preview: function(slug) {
                return $http.get('/api/artist/'+slug+'/preview',  {cache: true});
            }
        }
    });

    app.factory('Contact', function($http) {
        return {
            // get all the comments
            send: function(message) {
                return $http.post('/api/contact/send',message);
            }
        }
    });

    app.factory('Auth',function($http,SessionService){
        return{
            auth:function(credentials){
                return $http.post('api/artist/login',credentials);
            },
            logout:function(){
                return $http.get('api/artist/logout');
            },
            check: function(){
                var auth = false;
                if(SessionService.get('auth')){
                    $http.get('api/artist/login/check')
                        .success(function(){
                            auth = true;
                        });
                }
                return auth;
            }
        }
    });

    app.factory('SessionService',function(){
        return{
            get:function(key){
                return sessionStorage.getItem(key);
            },
            set:function(key,val){
                return sessionStorage.setItem(key,val);
            },
            unset:function(key){
                return sessionStorage.removeItem(key);
            }
        }
    });

    app.filter('phone', function () {
        return function (input) {
            return input.replace(/[^\+0-9]+/g, '');
        };
    });

    app.filter("sanitize", ['$sce', function($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        }
    }]);

})();
