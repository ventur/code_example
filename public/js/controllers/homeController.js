
angular.module('articonicApp')
    .controller('homeController', ['Artist','Artwork','$translatePartialLoader', homeController]);

    function homeController (Artist, Artwork) {

        this.tab = 1;
        this.selectTab = function(tab){
            this.tab = tab;
        }
        this.isSelected = function(tab) {
            return this.tab === tab;
        }

        var ctrl = this;
        ctrl.artworks = {};
        Artwork.latest().success(function(data) {
            ctrl.artworks = data;
        });
        ctrl.artists = {};
        Artist.latest().success(function(data) {
            ctrl.artists = data;
        });
    };
