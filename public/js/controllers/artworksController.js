
angular.module('articonicApp')
    .controller('artworksController', ['$scope','Artwork',artworksController]);

    function artworksController ($scope,Artwork) {

        $scope.artworks = {};
        $scope.pagination = {};
        $scope.factory = Artwork;

        Artwork.page(1).success(function (data) {
            $scope.pagination = data;
            $scope.artworks = data.data;
        });
    }
