
angular.module('articonicApp')
    .controller('artworkController', ['$scope','$routeParams','Artwork',artworkController]);

function artworkController ($scope,$routeParams,Artwork) {

    this.id =  $routeParams.id;
    $scope.artwork = null;

    Artwork.profile(this.id).success(function (data) {
        $scope.artwork = data;
    });
}
