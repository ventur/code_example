
angular.module('articonicApp')
    .controller('artistsController', ['$scope','$window','Artist',artistsController]);

    function artistsController($scope,$window,Artist) {

        $scope.artists = {};
        $scope.pagination = {};
        $scope.factory = Artist;

        $scope.preview = null;
        $scope.showPreview = showPreview;
        $scope.hidePreview = hidePreview;
        $scope.isPreview = isPreview;

        //Init
        Artist.page(1).success(function(data) {
            $scope.pagination = data;
            $scope.artists = data.data;
        });

        //Functions
        function isPreview() {
            return $scope.preview ? true : false;
        }
        function showPreview(slug) {
            if(angular.element($window).width()>1000){
                Artist.preview(slug).success(function(data){
                    $scope.preview = data;
                });
            }

        };
        function hidePreview() {
            $scope.preview = null;
        };
    };