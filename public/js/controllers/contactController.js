
angular.module('articonicApp')
    .controller('contactController', contactController);

    function contactController (Contact) {
        var ctrl = this;
        ctrl.message = {};
        ctrl.error = null;
        ctrl.success = null;
        ctrl.sendMessage = sendMessage;

        function sendMessage(){
            Contact.send(ctrl.message)
                .success(function(data) {
                    displaySuccess(data);
                })
                .error(function(data) {
                    displayError(data);
                });

            function displaySuccess(message) {
                ctrl.success = message;
                ctrl.error = null;
                ctrl.message = {};
            }

            function displayError(message) {
                ctrl.error = message;
                ctrl.success = null;
            }
        }
    }
