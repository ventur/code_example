
angular.module('articonicApp')
    .controller('loginController',['Auth','SessionService',loginController]);

function loginController (Auth,SessionService) {

    this.loginSubmit = loginSubmit;
    this.message = {};

    function loginSubmit(){
        Auth.auth(this.message)
            .success(function(response){
                if(response.id){
                    SessionService.set('auth',response.id);
                }else{
                    alert('could not verify your login');
                }
            });
        return true;
    }
}
