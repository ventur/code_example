
angular.module('articonicApp')
    .controller('galleryController', ['$scope','$routeParams','Artwork','Artist',galleryController]);

    function galleryController($scope, $routeParams,Artwork,Artist) {

        this.slug =  $routeParams.slug;
        $scope.artworks = {};
        $scope.artist = {};
        $scope.noInfo = noInfo;
        $scope.walk = [];
        $scope.startWalk = startWalk;

        Artist.profile(this.slug).success(function(data){
            $scope.artist = data;
            $scope.noInfo = noInfo();
        });

        //Init
        Artwork.gallery(this.slug).success(function (data) {
            $scope.artworks = data;

            angular.forEach($scope.artworks, function(value, key) {
                this.push({
                    href:'upload/artworks/'+value.id+'/large/'+value.picture,
                    title:value.name
                });
            }, $scope.walk);
        });

        function noInfo() {
            return $scope.artist.description ? false : true;
        };

        function startWalk() {
            $.swipebox($scope.walk);
        }
    };