// Welcome to the gulpfile! \o/
//
// Well, there’s nothing really interesting to see here. Tasks are
// organized in different files placed in the `./task` subdirectory.
//
// The npm page of each gulp plugin can be found at the following
// URL pattern: https://www.npmjs.org/package/{plugin-name}


// Load tasks from the subfolder
require('require-dir')('./tasks');
